package jpa.restfull;

import java.io.PrintStream;

public class SuperLogger {
	static PrintStream printStream = System.out;
	static PrintStream printStreamVoid= new PrintStream(new VoidOutputStream(),true);
	
	public static void println(Object obj) {
		System.setOut(printStream);
		printStream.println(obj);
		System.setOut(printStreamVoid);
	}
}
