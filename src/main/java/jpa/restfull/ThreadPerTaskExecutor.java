/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull;

import java.util.concurrent.Executor;

/**
 *
 * @author david
 */
public class ThreadPerTaskExecutor implements Executor{

    @Override
    public void execute(Runnable command) {
       new Thread(command).start();
    }
    
}
