package jpa.restfull;

import java.io.IOException;
import javax.persistence.EntityManager;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import jpa.restfull.model.APICall;
import jpa.restfull.model.Select;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.client.ResultatInstallation;
import org.json.JSONArray;

public class Application implements HttpHandler {

    static public HttpServer server;
    public ObjectMapper objectMapper = new ObjectMapper();
    public Engine engine;
    final public static String AdminKeyHeader = "adminkey";

    public static String POST = "POST";
    public static String GET = "GET";
    public static String sourceModel;

    static public void demarer(Model model, int port, String adminKey) throws Exception {
        Application application = new Application();
        server = HttpServer.create(new InetSocketAddress(port), 0);

        server.createContext("/", application);

        server.setExecutor(new ThreadPerTaskExecutor());
        server.start();
        Install install = new Install();
        install.model = model;
        install.pckg = model.url;
        application.engine = new Engine(install);
        application.engine.adminKey = adminKey;
        SuperLogger.println("<Demarer>");
        //System.out.flush();

    }

    public static void main(String[] args)
            throws Exception {
        SuperLogger.println("<Install>");
        int port = 80;
        Model model = null;
        String adminKey = null;
        if (args.length > 0) {
            System.out.println(args[0]);
            JSONObject jsonObject = new JSONObject(new String(Files.readAllBytes(Paths.get(args[0]))));
            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);

        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);

        }
        if (args.length > 2) {
            adminKey = args[2];
        }
        demarer(model, port, adminKey);

    }

    public String readString(InputStream is, int contentLength) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] bytes = new byte[contentLength];
        is.read(bytes);
        return new String(bytes);
    }

    public void apiUpdate(Install install) throws Exception {
        synchronized (this) {
            if (install.model.model.types.isEmpty()) {
                return;
            }
            EntityManager em = engine.factory.getEntityManager();
            engine.updateApi(install, em);
        }
    }

    public String process(String url, String method, String body, String adminKeyValue)
            throws Exception {

        if (url.equals("/echo")) {
            return "Echo";
        }

        boolean isAdmin = engine.adminKey.equals(adminKeyValue);
        if (url.equals(engine.urlPersist) && POST.equals(method)) {

            return persist(body, isAdmin);

        }
        if (url.equals(engine.urlSelectDeep) && POST.equals(method) && isAdmin) {
            return selectDeep(body);

        }
        if (url.equals(engine.urlSelectSimple) && POST.equals(method) && isAdmin) {
            return selectSimple(body);

        }
        if (url.equals(engine.urlClient) && (POST.equals(method) || GET.equals(method))) {
            return client();
        }

        if (url.equals(engine.urlApi) && POST.equals(method)) {
            return api(body);
        }

        if (isAdmin && url.equals(engine.urlApiUpdate) && POST.equals(method)) {
            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            Install install = (Install) contextJsonToJava.create(new JSONObject(body), "jpa.restfull.model.json.object", Install.class);
            apiUpdate(install);
            return client();
        }

        return "Pas de traitement pour " + url + " " + method;
    }

    public String persist(String jsonSrc, boolean isAdmin)
            throws Exception {

        JSONObject jsonObject = new JSONObject(jsonSrc);
        EntityManager em = engine.factory.getEntityManager();
        em.getTransaction().begin();
        Object r = engine.persist(em, jsonObject, isAdmin);
        em.getTransaction().commit();
        return r.toString();
    }

    public String selectDeep(String jsonSrc)
            throws ClassNotFoundException, JsonMappingException, JsonProcessingException, IllegalArgumentException,
            IllegalAccessException, JSONException, NoSuchFieldException, SecurityException {
        Select select = objectMapper.readValue(jsonSrc, Select.class);
        EntityManager em = engine.factory.getEntityManager();
        return engine.select(em, select, ContextEntityToJson.Mode.Deep).toString();

    }

    public String selectSimple(String jsonSrc)
            throws ClassNotFoundException, JsonMappingException, JsonProcessingException, IllegalArgumentException,
            IllegalAccessException, JSONException, NoSuchFieldException, SecurityException {
        Select select = objectMapper.readValue(jsonSrc, Select.class);
        EntityManager em = engine.factory.getEntityManager();
        return engine.select(em, select, ContextEntityToJson.Mode.Simple).toString();

    }

    public String api(String jsonSrc)
            throws Exception {
        APICall apiCall = objectMapper.readValue(jsonSrc, APICall.class);
        EntityManager em = engine.factory.getEntityManager();
        Object r = engine.execute(em, apiCall);
        if (r != null) {
            return r.toString();
        }
        return null;

    }

    public String client() throws Exception {
        ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
        return contextJavaToJson.create(engine.client()).toString();
    }

    @Override
    public void handle(HttpExchange he) throws IOException {

        String url = he.getRequestURI().toString();
        String method = he.getRequestMethod();
        String body = null;
        if (POST.equals(method)) {
        int contentLength = Integer.parseInt(he.getRequestHeaders().getFirst("Content-Length"));
         body = readString(he.getRequestBody(), contentLength);
        }
        String adminKeyValue = he.getRequestHeaders().getFirst(AdminKeyHeader);
        //  SuperLogger.println("process "+url + " method "+method);

        String resultat = null;
        try {
            resultat = process(url, method, body, adminKeyValue);
            if (resultat == null) {
                he.sendResponseHeaders(404, 0);
                OutputStream os = he.getResponseBody();

                os.flush();
                os.close();
                return;
            }
        } catch (Exception ex) {

            JSONObject r = new JSONObject();
            r.put("classError", ex.getClass().getSimpleName());
            r.put("message", ex.getMessage());
            if (ex.getCause() != null) {
                r.put("origine", ex.getCause().getMessage());
            }
            JSONArray stacktrace = new JSONArray();
            for (StackTraceElement elt : ex.getStackTrace()) {
                stacktrace.put(elt.toString());
            }
            r.put("stacktrace", stacktrace);
            OutputStream os = he.getResponseBody();
            byte bytes[] = r.toString().getBytes();
            he.sendResponseHeaders(500, bytes.length);
            os.write(bytes);

            he.getResponseHeaders().add("Content-Type", "applicaion/json");
            os.flush();
            os.close();
            return;
        }
        if (resultat == null) {
            return;
        }

        OutputStream os = he.getResponseBody();
        byte bytes[] = resultat.getBytes();
        he.sendResponseHeaders(200, bytes.length);
        os.write(bytes);

        he.getResponseHeaders().add("Content-Type", "applicaion/json");
        os.flush();
        os.close();

    }
}
