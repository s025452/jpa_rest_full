/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CustomClassLoader extends ClassLoader {

    public static Set<String> names = new HashSet<String>();
    public static Map<String, byte[]> classes = new HashMap<>();

    @Override
    public Class findClass(String name) throws ClassNotFoundException {
        /* synchronized (names) {
            this.names.add(name);
        } */
        byte[] b = loadClassFromFile(name);
        return defineClass(name, b, 0, b.length);
    }

    public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        synchronized (names) {
            byte[] b = classes.get(name);
            if (b != null) {
                try {
            //   return defineClass(name, b, 0, b.length); 
                } catch(Exception e) {
                   e.printStackTrace();
               }
            }
            this.names.add(name);
        }
        return super.loadClass(name, resolve);
    }

    private byte[] loadClassFromFile(String fileName) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(
                fileName.replace('.', File.separatorChar) + ".class");
        byte[] buffer;
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int nextValue = 0;
        try {
            while ((nextValue = inputStream.read()) != -1) {
                byteStream.write(nextValue);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        buffer = byteStream.toByteArray();
        return buffer;
    }
}
