package jpa.restfull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;

import org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl;
import org.hibernate.jpa.boot.internal.PersistenceUnitInfoDescriptor;
import org.springframework.boot.jdbc.DataSourceBuilder;


public class JpaEntityManagerFactory {

    public static String auto = "update";
    public String dataSourceUrl;
    public String dataSourceDriver;

    public JpaEntityManagerFactory() {

    }

    public EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

    protected EntityManagerFactory getEntityManagerFactory() {
        PersistenceUnitInfo persistenceUnitInfo = getPersistenceUnitInfo(getClass().getSimpleName());
        
        Map<String, Object> properties = new HashMap<>();
             EntityManagerFactoryBuilderImpl factory = new EntityManagerFactoryBuilderImpl(new PersistenceUnitInfoDescriptor(persistenceUnitInfo),
                properties);
                
        EntityManagerFactory r = factory.build();
        return r;
    }

    protected HibernatePersistenceUnitInfo getPersistenceUnitInfo(String name) {
        return new HibernatePersistenceUnitInfo(name, getProperties(), null);
    }

    protected Properties getProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        //properties.put("hibernate.id.new_generator_mappings", false);
        properties.put("hibernate.connection.datasource", getMysqlDataSource());
        properties.put("hibernate.hbm2ddl.auto", auto);
     
        return properties;
    }

    protected DataSource getMysqlDataSource() {
        // DriverManagerDataSource dataSource = new DriverManagerDataSource();
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        
        if (dataSourceDriver == null) {
            dataSourceBuilder.driverClassName("org.h2.Driver");

        } else {
            dataSourceBuilder.driverClassName(dataSourceDriver);
        }

        if (dataSourceUrl == null) {
            dataSourceBuilder.url("jdbc:h2:mem:test");
        } else {
            dataSourceBuilder.url(dataSourceUrl);

            //dataSourceBuilder.url("jdbc:h2:file:d:/tmp/test.db");
        }

        // dataSourceBuilder.username("SA");
        // dataSourceBuilder.password("");
        DataSource dataSource=  dataSourceBuilder.build();
        return dataSource;
                

    }
}
