/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.ZipOutputStream;
import jpa.restfull.EntityTree;
import jpa.restfull.HibernatePersistenceUnitInfo;

/**
 *
 * @author david
 */
public class EntityTreeHttpHandler implements HttpHandler {

    public static HttpServer server;
    public byte[] bytes;

    EntityTreeHttpHandler(EntityTree entityTree) throws IOException {
        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        entityTree.zipFile(null, zipOut);
        zipOut.close();
        fos.close();
        bytes = fos.toByteArray();
    }

    @Override
    public void handle(HttpExchange t) throws IOException {
        byte r[] = bytes;
        t.sendResponseHeaders(200, r.length);
        OutputStream os = t.getResponseBody();
        os.write(r);
        os.flush();
        os.close();
    }

    static public void demarer(EntityTree entityTree) throws IOException {
     
        EntityTreeHttpHandler httpHandler = new EntityTreeHttpHandler(entityTree);
        if (server != null) {
            server.stop(0);
        }
        server = HttpServer.create(new InetSocketAddress(6666), 0);

        server.createContext("/", httpHandler);

        server.setExecutor(null); // creates a default executor
        server.start();
        HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();
        HibernatePersistenceUnitInfo.JarFileUrls.add(new URL("http://localhost:6666/"));
    }

}
