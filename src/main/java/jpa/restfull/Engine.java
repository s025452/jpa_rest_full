package jpa.restfull;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javassist.CannotCompileException;
import javassist.ClassPool;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.transaction.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.API;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.Select;
import jpa.restfull.model.exception.APIInconnu;
import jpa.restfull.model.exception.IdAbsent;
import jpa.restfull.model.exception.IdPresent;
import jpa.restfull.model.exception.InvalidParam;
import jpa.restfull.model.exception.InvalidType;
import jpa.restfull.model.exception.InvalidTypePourPersist;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.exception.NeedAdminToPersist;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToEntity;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.InstallApi;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.json.object.client.Client;
import jpa.restfull.model.json.object.client.Erreur;
import org.springframework.util.StringUtils;

public class Engine {

    public EntitiesDef entitiesDef;
    public APIDef apiDef;
    String pckg;
    public JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
    public String urlPersistAdmin;
    public String urlPersist;
    public String urlApi;
    public String urlApiUpdate;
    public String urlModel;
    public String urlSelectDeep;
    public String urlSelectSimple;
    public String urlClient;
    public String adminKey;
    public List<Erreur> erreurs = new ArrayList<>();

    public Engine(Install install) throws Exception {
        String baseUrl = install.model.url;
        Types types = install.model.model;
        urlPersist = "/persist";
        urlSelectDeep = "/select/deep";
        urlSelectSimple = "/select/simple";
        urlModel = "/model";
        urlApi = "/api";
        urlApiUpdate = "/api/update";
        urlClient = "/client";
        entitiesDef = new EntitiesDef(types);
        types.controllerType(entitiesDef, erreurs);
        if (!erreurs.isEmpty()) {
            return;
        }
        ClassPool classPool = new ClassPool();

        factory.dataSourceUrl = install.dataSourceUrl;
        factory.dataSourceDriver = install.dataSourceDriver;
        classPool.appendSystemPath();
        pckg = "model.jpa";
        if (install.pckg != null) {
            pckg = install.pckg;
        }
        adminKey = UUID.randomUUID().toString();

        if (types.types.isEmpty()) {
            apiDef = new APIDef(entitiesDef, new ArrayList<>(), null);
            return;
        }

        entitiesDef.generate(pckg, classPool);
        if (Application.server == null) {
            EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
        } else {
            EntityTreeHttpHandler entityTreeHttpHandler = new EntityTreeHttpHandler(entitiesDef.entityTree);
            String classes = ("/" + baseUrl + "/classes").replaceAll("//", "/");
            Application.server.createContext(classes, entityTreeHttpHandler);
            HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();
            HibernatePersistenceUnitInfo.JarFileUrls.add(new URL("http://localhost:" + Application.server.getAddress().getPort() + classes));

        }
        EntityManager entityManager = factory.getEntityManager();
        apiDef = new APIDef(entitiesDef, install.model.apis, entityManager);

    }

    public Engine(EntitiesDef entitiesDef, APIDef apiDef, String pckg) {
        this.entitiesDef = entitiesDef;
        this.apiDef = apiDef;
        this.pckg = pckg;

    }

    public void updateApi(Install install, EntityManager em) {
        apiDef = new APIDef(entitiesDef, install.model.apis, em);

    }

    public JSONArray persistEntities(EntityManager entityManager, JSONObject jsonObject, boolean isAdmin) throws Exception {
        JSONObject references = jsonObject.getJSONObject(Constantes.REFERENCES);
        Map<String, Map<String, Object>> map = new HashMap<>();
        for (String key : references.keySet()) {
            Class cls = this.entitiesDef.mapEntityDef.get(key).cls;
            JSONObject values = references.getJSONObject(key);
            Map<String, Object> mapValues = new HashMap<>();
            map.put(key, mapValues);
            for (String id : values.keySet()) {
                Object obj = cls.getConstructor().newInstance();
                mapValues.put(id, obj);
            }
        }
        ContextJsonToEntity contextJsonToEntity = new ContextJsonToEntity(entitiesDef, entityManager);
        contextJsonToEntity.references = map;
        JSONArray jsonArray = jsonObject.getJSONArray(Constantes.VALEUR);
        JSONArray resultat = new JSONArray();
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject elt = jsonArray.getJSONObject(i);
                String type = elt.getString(Constantes.TYPE);
                if (!entitiesDef.persist(type) && !isAdmin) {
                    throw new NeedAdminToPersist(type);
                }
                Object objToPersist = contextJsonToEntity.create(elt, pckg, null);
                entityManager.persist(objToPersist);
                Field field = objToPersist.getClass().getField(Constantes.ID);
                resultat.put(field.get(objToPersist));

            }
            for (Map.Entry<String, Map<String, Object>> e : map.entrySet()) {
                for (Map.Entry<String, Object> e2 : e.getValue().entrySet()) {
                    JSONObject tmp = references.getJSONObject(e.getKey()).getJSONObject(e2.getKey());
                    contextJsonToEntity.init(tmp, e2.getValue(), pckg);
                    entityManager.persist(e2.getValue());

                }
            }
        }
        return resultat;

    }

    public Object persist(EntityManager entityManager, JSONObject jsonObject, boolean isAdmin) throws Exception {
        ContextJsonToEntity contextJsonToEntity = new ContextJsonToEntity(entitiesDef, entityManager);

        String entity = jsonObject.optString(Constantes.TYPE);
        if (StringUtils.isEmpty(entity)) {
           return this.persistEntities(entityManager, jsonObject, isAdmin);
        } 

        if (!entitiesDef.persist(entity) && !isAdmin) {
            throw new NeedAdminToPersist(entity);
        }
        Object obj = contextJsonToEntity.create(jsonObject, pckg, null);
        entityManager.persist(obj);
        Field field = obj.getClass().getField(Constantes.ID);
        return field.get(obj);

    }

    public JSONArray select(EntityManager entityManager, Select select, ContextEntityToJson.Mode mode) throws IllegalArgumentException, IllegalAccessException, JSONException, NoSuchFieldException, SecurityException {
        Query query = entityManager.createQuery(select.query);
        if (select.params != null) {
            for (Map.Entry<String, Object> e : select.params.entrySet()) {
                query.setParameter(e.getKey(), e.getValue());
            }
        }

        ContextEntityToJson contextEntityToJson = new ContextEntityToJson(entitiesDef, entityManager, mode);
        return contextEntityToJson.create(query.getResultList());

    }

    public Object execute(EntityManager entityManager, APICall apiCall) throws Exception {
        EntityTransaction t = entityManager.getTransaction();
        API api = apiDef.recupererAPI(apiCall.type);
        Query q = entityManager.createQuery(api.getSource());
        if (apiCall.valeur != null) {
            for (Map.Entry<String, Object> e : apiCall.valeur.entrySet()) {
                String nomParam = e.getKey();
                String typeParam = (api.getApiDescription().getParameters().get(nomParam));
                if (typeParam == null) {
                    throw new InvalidParam(nomParam);
                }
                Object value = e.getValue();
                if (typeParam.equals("uuid")) {
                    value = UUID.fromString((String) value);
                }
                q.setParameter(nomParam, value);
            }
        }
        ContextEntityToJson contextEntityToJson = new ContextEntityToJson(entitiesDef, entityManager, api.getMode());
        contextEntityToJson.useIdForCycle = true;
        if (api.getApiDescription().getRetour().isEmpty()) {
            t.begin();
            q.executeUpdate();
            t.commit();
            return new JSONArray();
        }
        if (apiCall.idxPage != null) {
            q.setFirstResult(apiCall.idxPage);
        }
        if (apiCall.sizePage != null) {
            q.setMaxResults(apiCall.sizePage);
        }
        List<String> retour = api.getApiDescription().getRetour();
        if (api.getApiDescription().extraireObjet()) {

            if (retour.size() == 1) {
                JSONArray resultat = contextEntityToJson.create(q.getResultList());
                if (api.getApiDescription().estUnique) {
                    if (resultat.length() == 0) {
                        return null;
                    }
                    if (resultat.length() == 1) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Constantes.VALEUR, resultat.getJSONObject(0));
                        jsonObject.put(Constantes.REFERENCES, contextEntityToJson.references());
                        return jsonObject;

                    }
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constantes.VALEUR, resultat);
                jsonObject.put(Constantes.REFERENCES, contextEntityToJson.references());
                return jsonObject;

            }
            List<Object> objects = new ArrayList<>();
            List<Object> resultList = q.getResultList();

            for (Object obj : resultList) {
                Object[] array = (Object[]) obj;
                for (int idx = 0; idx < array.length; idx++) {
                    if (Constantes.estTypeEntity(retour.get(idx))) {
                        objects.add(array[idx]);
                    }
                }

            }
            JSONArray resultat = contextEntityToJson.create(objects);
            Map<Object, JSONObject> map = new HashMap<>();
            for (int idx = 0; idx < objects.size(); idx++) {
                map.put(objects.get(idx), resultat.getJSONObject(idx));
            }
            JSONArray jsonArray = new JSONArray();
            for (Object obj : resultList) {
                Object[] array = (Object[]) obj;
                JSONObject valeur = api.descriptionRetour.creerJSONObject(array, map);
                JSONObject o = new JSONObject();
                o.put(Constantes.VALEUR, valeur);
                o.put(Constantes.TYPE,Constantes.PREFIXE_REPONSE+apiCall.type);
                jsonArray.put(o);

            }
            /*    for (int idx = objects.size(); idx < resultat.length(); idx++) {
                jsonArray.put(resultat.getJSONObject(idx));
            }*/
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constantes.VALEUR, jsonArray);
            jsonObject.put(Constantes.REFERENCES, contextEntityToJson.references());
            return jsonObject;

        }
        List<Object> resultList = q.getResultList();
        JSONArray jsonArray = new JSONArray();
        for (Object obj : resultList) {
            Object[] array = (Object[]) obj;
            JSONObject jsonObject = api.descriptionRetour.creerJSONObject(array, null);
            jsonArray.put(jsonObject);

        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constantes.VALEUR, jsonArray);
        jsonObject.put(Constantes.REFERENCES, contextEntityToJson.references());
        return jsonObject;

    }

    public Client client() {
        Client client = new Client();
        client.types = clientTypes();
        client.signatures = this.apiDef.signatures(client);
        client.erreurs = this.apiDef.erreurs();
        return client;
    }

    public List<jpa.restfull.model.json.object.client.Type> clientTypes() {
        List<jpa.restfull.model.json.object.client.Type> r = new ArrayList<>();
        if (entitiesDef != null) {
            entitiesDef.recupererClientType(r);
        }
        if (apiDef != null) {
            apiDef.recupererClientType(r);
        }
        return r;

    }

}
