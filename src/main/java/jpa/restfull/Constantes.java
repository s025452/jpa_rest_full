package jpa.restfull;

public class Constantes {
	static public String VALEUR = "valeur";
	static public String TYPE = "type";
	static public String ID="id";
        static public String TYPE_NUMBER="number";
        static public String TYPE_STRING="string";
        static public String TYPE_UUID="uuid";
        static public String REFERENCES = "references";
        static public String PREFIXE_REPONSE="Reponse";
        
        public static boolean estTypeEntity(String type) {
            if (type.equals(TYPE_NUMBER)) {
                return false;
            }
            if (type.equals(TYPE_STRING)) {
                return false;
            }
            if (type.equals(TYPE_UUID)) {
                return false;
            }
            return true;
        }

}
