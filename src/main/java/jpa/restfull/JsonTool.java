package jpa.restfull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


import org.json.JSONArray;
import org.json.JSONObject;

public class JsonTool {
	static public JSONObject loadJSONObject(String chemin) throws IOException {
		InputStream isReader = JsonTool.class.getResourceAsStream(chemin);
	      StringBuffer sb = new StringBuffer();
	      String str;
	      int n=0;
	      byte bytes[] = new byte[512];
	      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	      while((n = isReader.read(bytes, 0, bytes.length)) > 0){
	         byteArrayOutputStream.write(bytes, 0, n);
	      }
	      String s = new String(byteArrayOutputStream.toByteArray());
		return new JSONObject(s);
	}
	static public JSONArray loadJSONArray(String chemin) throws IOException {
		InputStream isReader = JsonTool.class.getResourceAsStream(chemin);
	      StringBuffer sb = new StringBuffer();
	      String str;
	      int n=0;
	      byte bytes[] = new byte[512];
	      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	      while((n = isReader.read(bytes, 0, bytes.length)) > 0){
	         byteArrayOutputStream.write(bytes, 0, n);
	      }
	      String s = new String(byteArrayOutputStream.toByteArray());
		return new JSONArray(s);
	}	

}
