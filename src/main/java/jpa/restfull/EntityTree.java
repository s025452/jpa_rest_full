/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author david
 */
public class EntityTree {

    public byte bytes[];
    public Map<String, EntityTree> children = new HashMap<>();

    public void zipFile(String fileName, ZipOutputStream zipOut) throws IOException {

        if (bytes == null) {
            if (fileName != null) {
                    fileName = fileName + "/";
                zipOut.putNextEntry(new ZipEntry(fileName));
                
             
            } else {
              fileName = "";
            }
            for (Map.Entry<String, EntityTree> et : children.entrySet()) {
                et.getValue().zipFile(fileName + et.getKey(), zipOut);
            }
            zipOut.closeEntry();
            return;
        }
        ZipEntry zipEntry = new ZipEntry(fileName+".class");
        zipOut.putNextEntry(zipEntry);
      //  SuperLogger.println("entry " + fileName);
        zipOut.write(bytes, 0, bytes.length);
        zipOut.closeEntry();
    }

}
