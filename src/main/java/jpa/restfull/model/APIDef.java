package jpa.restfull.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import jpa.restfull.model.api.SelectDescriptionRetour;

import jpa.restfull.model.exception.APIInconnu;
import jpa.restfull.model.exception.TypeNonValidePourAPI;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Colonne;
import jpa.restfull.model.json.object.client.ApiSignature;
import jpa.restfull.model.json.object.client.Client;
import jpa.restfull.model.json.object.client.Erreur;
import org.json.JSONArray;

public class APIDef {

    public Map<String, API> mapAPI = new HashMap<>();

    public APIDef(EntitiesDef entitiesDef, List<APIRest> list, EntityManager entityManager) {

        for (APIRest apiRest : list) {
            ContextEntityToJson.Mode mode = ContextEntityToJson.Mode.Deep;
            if (apiRest.mode != null) {
                mode = ContextEntityToJson.Mode.valueOf(apiRest.mode);
            }
            if (Boolean.TRUE.equals(apiRest.useIndex)) {
                ajouter(apiRest,mode, entityManager, entitiesDef);
            } else {
                API api = ajouter(apiRest,mode, entityManager);
                SelectDescriptionRetour selectDescriptionRetour = api.descriptionRetour;
                if (selectDescriptionRetour != null) {
                    selectDescriptionRetour.initaliserEntityType(entitiesDef);
                }
            }
        }
    }

    public void ajouter(APIRest apiRest,   ContextEntityToJson.Mode mode,EntityManager em, EntitiesDef entitiesDef) {
        IndexAvecType indexAvecType = entitiesDef.indexes.get(apiRest.requete);
        if (indexAvecType == null) {
            API api = new API();
            api.messageErreur = " Index " + apiRest +" absent (format <nom type>:<nom index> )";
             mapAPI.put(apiRest.nom, api);
             return;
   
        }
        StringBuilder sb = new StringBuilder();
        sb.append(" Select rs from ");
        sb.append(indexAvecType.type);
        sb.append(" rs where ");
        boolean estPremier = true;
        for (Colonne col : indexAvecType.index.colonnes) {
            if (!estPremier) {
                sb.append(" and ");
            }
            estPremier = false;
            sb.append(" rs.");
            sb.append(col.nom);
            sb.append("= :");
            sb.append(col.nom);
        }
        API r = new API(em, sb.toString(), mode);
        if (r.apiDescription != null) {
        r.apiDescription.estUnique = indexAvecType.index.unique;
        }
        mapAPI.put(apiRest.nom, r);
   
    }

    public List<ApiSignature> signatures(Client client) {
        List<ApiSignature> signatures = new ArrayList<>();

        for (Map.Entry<String, API> e : mapAPI.entrySet()) {
            if (e.getValue().messageErreur == null) {
                ApiSignature apiSignature = new ApiSignature();
                apiSignature.typeQuestion = e.getKey();
                apiSignature.estUnique = e.getValue().apiDescription.estUnique;
                apiSignature.selectDescriptionRetour = e.getValue().descriptionRetour;
                if (apiSignature.selectDescriptionRetour != null) {
                    apiSignature.selectDescriptionRetour.creerTypeReponse(e.getKey(), apiSignature,client);
                }
                signatures.add(apiSignature);
            }

        }
        return signatures;
    }

    public List<Erreur> erreurs() {
        List<Erreur> erreurs = new ArrayList<>();
        for (Map.Entry<String, API> e : mapAPI.entrySet()) {

            if (e.getValue().messageErreur != null) {
                Erreur erreur = new Erreur();
                erreur.libelle = e.getValue().messageErreur;
                erreur.nom = e.getKey();
                erreurs.add(erreur);
            }
        }
        return erreurs;
    }

    public API recupererAPI(String nom) throws APIInconnu {
        API api = mapAPI.get(nom);
        if (api == null) {
            throw new APIInconnu();
        }
        return api;

    }

    public API ajouter(APIRest apiRest,  ContextEntityToJson.Mode mode, EntityManager entityManager) {
        API r = new API(entityManager, apiRest.requete, mode);
        mapAPI.put(apiRest.nom, r);
        return r;
        
    }

    public void recupererClientType(List<jpa.restfull.model.json.object.client.Type> types) {
        for (Map.Entry<String, API> e : mapAPI.entrySet()) {
            if (e.getValue().messageErreur == null) {
                types.add(e.getValue().getApiDescription().clientType(e.getKey()));
            }
        }
    }

}
