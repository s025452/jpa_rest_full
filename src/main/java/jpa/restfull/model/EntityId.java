package jpa.restfull.model;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.NotFoundException;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.EnumMemberValue;
import jpa.restfull.Constantes;

abstract public class EntityId {

    boolean generated;

    public Object convertir(Object obj) {
        return obj;
    }

    public boolean isGenerated() {
        return generated;
    }

    public void setGenerated(boolean generated) {
        this.generated = generated;
    }

    public void generate(ClassPool pool, CtClass ctClass, ConstPool constpool, ClassFile cfile)
            throws CannotCompileException, NotFoundException {

        CtField field = new CtField(pool.get(this.classId().getName()), idFieldName(), ctClass);
        FieldInfo fieldInfo = field.getFieldInfo();
        Annotation annotation1 = null;
        if (generated) {
            annotation1 = new Annotation("javax.persistence.GeneratedValue", constpool);
            EnumMemberValue enumMemberValue = new EnumMemberValue(constpool);
            enumMemberValue.setType("javax.persistence.GenerationType");
            enumMemberValue.setValue("AUTO");
            annotation1.addMemberValue("strategy", enumMemberValue);
        }
        Annotation annotation2 = new Annotation("javax.persistence.Id", constpool);
        AnnotationsAttribute annotationsAttribute = new AnnotationsAttribute(constpool,
                AnnotationsAttribute.visibleTag);

        if (generated) {
            annotationsAttribute.setAnnotations(new Annotation[]{annotation1, annotation2});

        } else {
            annotationsAttribute.setAnnotations(new Annotation[]{annotation2});

        }
        fieldInfo.addAttribute(annotationsAttribute);

        fieldInfo.setAccessFlags(AccessFlag.PUBLIC);

        cfile.addField(fieldInfo);

    }

    abstract public Class classId();

    public String idFieldName() {
        return Constantes.ID;
    }

    abstract public String nomType();

}
