package jpa.restfull.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import jpa.restfull.Constantes;

import org.hibernate.engine.query.spi.NamedParameterDescriptor;
import org.hibernate.query.internal.QueryImpl;
import org.hibernate.type.ManyToOneType;

import jpa.restfull.model.exception.TypeNonValidePourAPI;


public class APIDescription {
	
	 Map<String,String> parameters = new HashMap<>();
	 List<String> retour = new ArrayList<>();
         
         public boolean estUnique = false;
         
         public boolean extraireObjet() {
         
             for(String type:retour) {
                 if (!type.equals(Constantes.TYPE_NUMBER) && !type.equals(Constantes.TYPE_NUMBER)) {
                     return true;
                 }
             }
             return false;
             
         }
	 
	public APIDescription(QueryImpl<?> q) throws TypeNonValidePourAPI {
		for(Parameter<?> p: q.getParameters()) {
			NamedParameterDescriptor npd = (NamedParameterDescriptor) p; 
			org.hibernate.type.Type type = npd.getExpectedType();	
			parameters.put(npd.getName(), APIDescription.nomDuType(type));			
			
		}
		 org.hibernate.type.Type[] returnTypes =  q.getReturnTypes();
                 if (returnTypes != null) {
		 for(org.hibernate.type.Type type:returnTypes) {
			 retour.add(APIDescription.nomDuType(type));
		 }
                 }
	
		
	}

	
	public Map<String, String> getParameters() {
		return parameters;
	}


	public List<String> getRetour() {
		return retour;
	}


	static public String nomDuType( org.hibernate.type.Type type) throws TypeNonValidePourAPI {
		if (type instanceof org.hibernate.type.IntegerType) {
			return Constantes.TYPE_NUMBER;
		}
		if (type instanceof org.hibernate.type.StringType) {
			return Constantes.TYPE_STRING;
		}
		if (type instanceof org.hibernate.type.UUIDBinaryType) {
			return "uuid";
		}
		if (type instanceof org.hibernate.type.ManyToOneType) {
			org.hibernate.type.ManyToOneType tmp = (ManyToOneType) type;
			String name = tmp.getAssociatedEntityName();
			return name.substring(name.lastIndexOf('.')+1);
		}
		
		throw new TypeNonValidePourAPI(type);
		
	}
         public jpa.restfull.model.json.object.client.Type clientType(String nom) {
            jpa.restfull.model.json.object.client.Type r = new jpa.restfull.model.json.object.client.Type();
            r.abstrait = false;
            r.nom = nom;
            r.champs  =  new ArrayList<>();
            for(String nomChamp:parameters.keySet())  {
                jpa.restfull.model.json.object.client.Champ champ = new jpa.restfull.model.json.object.client.Champ();
                champ.nom = nomChamp;
                champ.type = this.parameters.get(nomChamp);
                if (champ.type.equals("uuid")) {
                    champ.type = "string";
                }
                r.champs.add(champ);
            }
            return r;
            
            
        }
}
