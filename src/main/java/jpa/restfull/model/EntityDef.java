package jpa.restfull.model;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.ByteArrayClassPath;

import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtNewConstructor;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.AnnotationMemberValue;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.EnumMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import jpa.restfull.CustomClassLoader;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.model.exception.InvalidField;
import jpa.restfull.model.exception.InvalidType;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.object.Index;
import jpa.restfull.model.json.object.client.Champ;
import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeBoolean;
import jpa.restfull.model.type.EntityFieldTypeInteger;
import jpa.restfull.model.type.EntityFieldTypeNumber;
import jpa.restfull.model.type.EntityFieldTypeRef;
import jpa.restfull.model.type.EntityFieldTypeString;

public class EntityDef {

    public boolean abstrait;
    public Boolean persist;
    public String superEntityDef;
    public EntityId entityId;
    public Class cls;
    public byte[] bytesCode;
    public CtClass ctClass;
    public boolean generated = false;
    public boolean needType = false;
    public List<Index> indexes = new ArrayList<>();
    public List<String> declaredFields = new ArrayList<String>();
    public List<String> allFields = new ArrayList<String>();

    public jpa.restfull.model.json.object.client.Type clientType(String nom) {
        jpa.restfull.model.json.object.client.Type r = new jpa.restfull.model.json.object.client.Type();
        r.abstrait = abstrait;
        r.superType = superEntityDef;
        r.nom = nom;
        r.champs = new ArrayList<>();
        for (String nomChamp : allFields) {
            jpa.restfull.model.json.object.client.Champ champ = new jpa.restfull.model.json.object.client.Champ();
            champ.nom = nomChamp;
            champ.type = this.champs.get(nomChamp).clientType();
            r.champs.add(champ);
        }
        jpa.restfull.model.json.object.client.Champ champ = new jpa.restfull.model.json.object.client.Champ();
        champ.nom = this.entityId.idFieldName();
        champ.type = this.entityId.nomType();
        r.champs.add(champ);
        return r;

    }

    public void generate(EntitiesDef entitiesDef, String entityName, ClassPool classPool, String pckg)
            throws CannotCompileException, NotFoundException, IOException {
        if (generated) {
            return;
        }

        String className = pckg + "." + entityName;
        ctClass.setName(className);
        ctClass.defrost();
        if (this.superEntityDef != null) {
            EntityDef se = entitiesDef.mapEntityDef.get(this.superEntityDef);
            //se.generate(entitiesDef, entityName, classPool, path, pckg);
            ctClass.setSuperclass(se.ctClass);
        }

        CtConstructor defaultConstructor = CtNewConstructor.make("public " + ctClass.getSimpleName() + "() {}", ctClass);
        ctClass.addConstructor(defaultConstructor);
        ClassFile cfile = ctClass.getClassFile();
        ConstPool constpool = cfile.getConstPool();
        AnnotationsAttribute annotationsAttribute = new AnnotationsAttribute(constpool,
                AnnotationsAttribute.visibleTag);
        Annotation annotation = new Annotation("javax.persistence.Entity", constpool);
        MemberValue mv = new StringMemberValue(entityName, constpool);
        annotation.addMemberValue("name", mv);
        Annotation annotationTable = creerAnnotationTable(constpool, entityName);

        if (superEntityDef == null) {
            Annotation annotation2 = new Annotation("javax.persistence.Inheritance", constpool);
            EnumMemberValue enumMemberValue = new EnumMemberValue(constpool);
            enumMemberValue.setType("javax.persistence.InheritanceType");
            enumMemberValue.setValue("TABLE_PER_CLASS");
            annotation2.addMemberValue("strategy", enumMemberValue);
            annotationsAttribute.setAnnotations(new Annotation[]{annotation, annotation2, annotationTable});
        } else {

            annotationsAttribute.setAnnotations(new Annotation[]{annotation, annotationTable});

        }
        cfile.addAttribute(annotationsAttribute);
        if (entityId != null) {
            entityId.generate(classPool, ctClass, constpool, cfile);
        }
        for (String n : this.declaredFields) {
            EntityFieldType e = this.champs.get(n);
            e.generate(pckg, n, classPool, ctClass, constpool, cfile);
        }
        generated = true;
    }

    public Annotation creerAnnotationTable(ConstPool constpool, String entityName) {
        Annotation annotation = new Annotation("javax.persistence.Table", constpool);
        MemberValue mv = new StringMemberValue(entityName, constpool);
        annotation.addMemberValue("name", mv);
        ArrayMemberValue arrayMemberValue = new ArrayMemberValue(constpool);
        MemberValue memberValues[] = new MemberValue[this.indexes.size()];
        for (int i = 0; i < memberValues.length; i++) {
            AnnotationMemberValue annotationMemberValue = new AnnotationMemberValue(indexes.get(i).creerAnnotation(this, constpool), constpool);
            memberValues[i] = annotationMemberValue;
        }
        arrayMemberValue.setValue(memberValues);
        annotation.addMemberValue("indexes", arrayMemberValue);
        return annotation;

    }

    public void buildClass(EntitiesDef entitiesDef) throws CannotCompileException, IOException {
        if (cls != null) {
            return;
        }
        if (this.superEntityDef != null) {
            entitiesDef.mapEntityDef.get(this.superEntityDef).buildClass(entitiesDef);
        }
        String name = ctClass.getName();
        this.bytesCode = ctClass.toBytecode();
        entitiesDef.addEntity(name, bytesCode);
        cls = ctClass.toClass();

    }

    public void initFields() {
        Field fields[] = cls.getFields();
        for (Field field : fields) {
            this.champs.get(field.getName()).field = field;
        }
    }

    public Map<String, EntityFieldType> champs = new HashMap<>();

    public boolean isAbstrait() {
        return abstrait;
    }

    public void setAbstrait(boolean abstrait) {
        this.abstrait = abstrait;
    }

    public String getSuperEntityDef() {
        return superEntityDef;
    }

    public void setSuperEntityDef(String superEntityDef) {
        this.superEntityDef = superEntityDef;
    }

    public Map<String, EntityFieldType> getChamps() {
        return champs;
    }

    public void setChamps(Map<String, EntityFieldType> champs) {
        this.champs = champs;
    }

    public EntityId getEntityId() {
        return entityId;
    }

    public void setEntityId(EntityId entityId) {
        this.entityId = entityId;
    }

    public boolean isNeedType() {
        return needType;
    }

    public void setNeedType(boolean needType) {
        this.needType = needType;
    }

    public EntityDef() {

    }

}
