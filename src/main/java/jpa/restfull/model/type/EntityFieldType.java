package jpa.restfull.model.type;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.persistence.EntityManager;

import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ControlException;
import jpa.restfull.model.exception.InvalidField;
import jpa.restfull.model.exception.InvalidType;

public abstract class EntityFieldType {
	public Field field;

	abstract public void generate(String pckgName,String fieldName,ClassPool pool, CtClass ctClass, ConstPool constpool, ClassFile cfile)
			throws CannotCompileException, NotFoundException ;

	public boolean declareField() {
		return true;
	}
	public void init(EntitiesDef entitiesDef, String type) {
		
	}
        public void controlJSON(JSONObject jsonObject , String field) throws ControlException {
            
            
        }
        abstract public String clientType(); 

}