package jpa.restfull.model.type;

import java.lang.reflect.InvocationTargetException;

import javax.persistence.EntityManager;

import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.exception.InvalidField;
import jpa.restfull.model.exception.InvalidType;

public class EntityFieldTypeList extends EntityFieldType {
	public String nom;
	public String lienInverse;
	public EntityFieldTypeList(String nom,String lienInverse) {
		this.nom = nom;
		this.lienInverse = lienInverse;
	}

	@Override
	public void generate(String pckgName, String fieldName, ClassPool pool, CtClass ctClass, ConstPool constpool,
			ClassFile cfile) throws CannotCompileException, NotFoundException {
		// TODO Auto-generated method stub

	}


	public boolean declareField() {
		return false;
	}

    @Override
    public String clientType() {
        return "*"+nom;
    }

}
