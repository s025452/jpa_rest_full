package jpa.restfull.model.type;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.NotFoundException;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.annotation.Annotation;
import jpa.restfull.Constantes;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ControlException;
import jpa.restfull.model.exception.InvalidField;
import jpa.restfull.model.exception.InvalidType;

public class EntityFieldTypeRef extends EntityFieldTypeWithGeneration {

    String type;
    boolean needType;

    public boolean isNeedType() {
        return needType;
    }

    public void setNeedType(boolean needType) {
        this.needType = needType;
    }

    boolean noInsert;

    public boolean isNoInsert() {
        return noInsert;
    }

    public void setNoInsert(boolean noInsert) {
        this.noInsert = noInsert;
    }

    public boolean isNoUpdate() {
        return noUpdate;
    }

    public void setNoUpdate(boolean noUpdate) {
        this.noUpdate = noUpdate;
    }
    boolean noUpdate;

    public EntityFieldTypeRef(String type, boolean noInsert, boolean noUpdate,boolean notNull) {
        this.noInsert = noInsert;
        this.noUpdate = noUpdate;

        this.type = type;
        this.notNull = notNull;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void generate(String pckgName, String fieldName, ClassPool pool, CtClass ctClass, ConstPool constpool,
            ClassFile cfile) throws CannotCompileException, NotFoundException {
        // TODO Auto-generated method stub

        CtField field = new CtField(pool.get(pckgName + "." + type), fieldName, ctClass);
        FieldInfo fieldInfo = field.getFieldInfo();
        fieldInfo.setAccessFlags(AccessFlag.PUBLIC);
        Annotation annotation = new Annotation("javax.persistence.ManyToOne", constpool);
           this.ajouterNotNull(fieldInfo, constpool, annotation);
cfile.addField(fieldInfo);
    }



    public void controlJSON(JSONObject jsonObject, String field) throws ControlException {
        super.controlJSON(jsonObject, field);
        if (!jsonObject.has(field)) {
        
            return;
        }
        JSONObject valeur = jsonObject.getJSONObject(field);
        if (needType) {
           valeur = jsonObject.getJSONObject(Constantes.VALEUR);
        }
        if (this.noInsert) {
            if (!valeur.has(Constantes.ID)) {
                throw new  ControlException(jsonObject,field,this);
            }
        }
        if (this.noUpdate) {
            if (valeur.has(Constantes.ID)) {
                if (valeur.length() >  1) {
                throw new  ControlException(jsonObject,field,this); }
            }
        }
        
    }

    public void init(EntitiesDef entitiesDef, String type) {
        EntityDef entityDef = entitiesDef.mapEntityDef.get(this.type);
        if (entityDef == null) {
            return;
        }
        this.needType = entityDef.isNeedType();
    }

    @Override
    public String clientType() {
        return this.type;
    }
}
