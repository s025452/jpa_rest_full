package jpa.restfull.model.type;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.persistence.EntityManager;

import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.InvalidField;
import jpa.restfull.model.exception.InvalidType;

public class EntityFieldTypeListRelation extends EntityFieldType {
    public String nom;
    public String relation;
    public String colonneSource;
    public String colonneDestination;

    public EntityFieldTypeListRelation(String nom, String relation) {
        this.nom = nom;
        this.relation = relation;
    }

    @Override
    public void generate(String pckgName, String fieldName, ClassPool pool, CtClass ctClass, ConstPool constpool,
                         ClassFile cfile) throws CannotCompileException, NotFoundException {
        // TODO Auto-generated method stub

    }


    public boolean declareField() {
        return false;
    }

    public void init(EntitiesDef entitiesDef, String type) {
        EntityDef destination = entitiesDef.mapEntityDef.get(relation);
        for (Map.Entry<String, EntityFieldType> e : destination.champs.entrySet()) {
            EntityFieldType eft = e.getValue();
            if (eft instanceof EntityFieldTypeRef) {
                EntityFieldTypeRef entityFieldTypeRef = (EntityFieldTypeRef) eft;
                if (entityFieldTypeRef.type.equals(nom)) {
                    this.colonneDestination = e.getKey();
                }
                if (entityFieldTypeRef.type.equals(type)) {
                    this.colonneSource = e.getKey();
                }
            }
        }

    }

    @Override
    public String clientType() {
        return "*" + nom;
    }
}
