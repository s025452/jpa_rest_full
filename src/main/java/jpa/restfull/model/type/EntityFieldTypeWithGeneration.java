/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.type;

import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.BooleanMemberValue;
import javassist.bytecode.annotation.EnumMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import jpa.restfull.Constantes;
import jpa.restfull.model.exception.ControlException;
import org.json.JSONObject;

/**
 *
 * @author DAVID
 */
abstract public class EntityFieldTypeWithGeneration extends EntityFieldType {


    boolean notNull;

    public boolean isNotNull() {
        return notNull;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public void ajouterNotNull(FieldInfo fieldInfo, ConstPool constpool, Annotation... annotations) {
        Annotation array[] = annotations;
      /*  if (notNull) {
           
            Annotation annotation2 = new Annotation("javax.persistence.Column", constpool);
            MemberValue mv = new BooleanMemberValue(false, constpool);
		annotation2.addMemberValue("nullable", mv);
            array = new Annotation[annotations.length + 1];
            array[0] = annotation2;
            for (int i = 0; i < annotations.length; i++) {
                array[i + 1] = annotations[i];
            }

        }*/
        if (array.length == 0) {
            return;
        }

        AnnotationsAttribute annotationsAttribute = new AnnotationsAttribute(constpool,
                AnnotationsAttribute.visibleTag);

        annotationsAttribute.setAnnotations(array);

        fieldInfo.addAttribute(annotationsAttribute);

    }
    public void controlJSON(JSONObject jsonObject , String field) throws ControlException {
       JSONObject valeur = jsonObject;
         if (this.notNull) {
            if (valeur.has(Constantes.ID)) {
                if (!valeur.has(field)) {
                    return;
                }
                Object obj = jsonObject.get(field);
                if (obj == null) {
                    throw new ControlException(jsonObject, field, this);
                }

            } else {
                if (!valeur.has(field)) {
                    throw new ControlException(jsonObject, field, this);
                }
                Object obj = jsonObject.get(field);
                if (obj == null) {
                    throw new ControlException(jsonObject, field, this);
                }
                
            }

        }   
        }

}
