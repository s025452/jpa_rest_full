package jpa.restfull.model.type;

import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.NotFoundException;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.exception.InvalidField;

public class EntityFieldTypeBoolean extends EntityFieldTypeWithGeneration {

    public EntityFieldTypeBoolean(boolean notNull) {
        this.notNull = notNull;
    }

	@Override
	public void generate(String pckgName,String fieldName, ClassPool pool, CtClass ctClass, ConstPool constpool, ClassFile cfile)
			throws CannotCompileException, NotFoundException {
		// TODO Auto-generated method stub
		CtField field = new CtField(pool.get(Boolean.class.getName()), fieldName, ctClass);
		FieldInfo fieldInfo = field.getFieldInfo();
		fieldInfo.setAccessFlags(AccessFlag.PUBLIC);
                this.ajouterNotNull(fieldInfo, constpool);
                cfile.addField(fieldInfo);

	}



    @Override
    public String clientType() {
        return "boolean";
        
    }

}
