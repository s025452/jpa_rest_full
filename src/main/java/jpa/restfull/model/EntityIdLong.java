package jpa.restfull.model;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.NotFoundException;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.EnumMemberValue;

public class EntityIdLong extends EntityId {

	@Override
	public Class classId() {
		// TODO Auto-generated method stub
		return Long.class;
	}

    @Override
    public String nomType() {
        return "number";
    }
	
}
