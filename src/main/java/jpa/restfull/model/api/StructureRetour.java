/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jpa.restfull.Constantes;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeRef;
import org.json.JSONObject;

/**
 *
 * @author david
 */
@JsonInclude(Include.NON_NULL)
public class StructureRetour {

    public String entityType;
    public Integer idx;
    @JsonInclude(Include.NON_EMPTY)
    public Map<String, StructureRetour> valeurs = new HashMap<>();

    public void ajouter(int idxPos, String[] chemin, int idx) {
        StructureRetour sr = valeurs.get(chemin[idxPos]);
        if (sr == null) {
            sr = new StructureRetour();
            this.valeurs.put(chemin[idxPos], sr);
        }
        if (idxPos + 1 == chemin.length) {
            sr.idx = idx;
        } else {
            sr.ajouter(idxPos + 1, chemin, idx);
        }

    }

    public void initialiserJSONObject(String keyParent, JSONObject r, Object[] objects, Map<Object, JSONObject> map) {

        if (this.idx != null) {
            if (this.entityType != null) {
                r.put(keyParent, map.get(objects[this.idx]));
            } else {
                r.put(keyParent, objects[this.idx]);
            }
            return;
        }
        if (this.entityType != null) {
            JSONObject jo = new JSONObject();
            JSONObject valeur = new JSONObject();
            jo.put(Constantes.TYPE, entityType);
            jo.put(Constantes.VALEUR, valeur);
            r.put(keyParent, jo);
            for (Map.Entry<String, StructureRetour> e : this.valeurs.entrySet()) {
                e.getValue().initialiserJSONObject(e.getKey(), valeur, objects, map);
            }
        }
    }


    public void initaliserEntityType(EntitiesDef entitiesDef) {
        if (entityType == null) {
            return;
        }
        EntityDef entityDef = entitiesDef.mapEntityDef.get(entityType);
        if (entityDef == null) {
            return;
        }
        for (Map.Entry<String, StructureRetour> e : this.valeurs.entrySet()) {
            EntityFieldType type = entityDef.champs.get(e.getKey());
            if (type != null && type instanceof EntityFieldTypeRef) {
                EntityFieldTypeRef entityFieldTypeRef = (EntityFieldTypeRef) type;
                e.getValue().entityType = entityFieldTypeRef.getType();
                e.getValue().initaliserEntityType(entitiesDef);
            }

        }

    }
}
