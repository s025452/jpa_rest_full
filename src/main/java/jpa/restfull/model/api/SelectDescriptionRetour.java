/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.object.client.ApiSignature;
import jpa.restfull.model.json.object.client.Champ;
import jpa.restfull.model.json.object.client.Client;
import jpa.restfull.model.json.object.client.Type;
import org.eclipse.persistence.jpa.jpql.parser.Expression;
import org.eclipse.persistence.jpa.jpql.parser.JPQLExpression;
import org.eclipse.persistence.jpa.jpql.parser.JPQLGrammar2_1;
import org.eclipse.persistence.jpa.jpql.parser.SelectClause;
import org.json.JSONObject;

/**
 *
 * @author david
 */
public class SelectDescriptionRetour {

    public Map<String, String> from = new HashMap<>();
    public Map<String, StructureRetour> retour = new HashMap<>();

    public SelectDescriptionRetour() {

    }

    public void initaliserEntityType(EntitiesDef entitiesDef) {
        for (Map.Entry<String, StructureRetour> e : retour.entrySet()) {
            e.getValue().entityType = from.get(e.getKey());

        }

    }

    public JSONObject creerJSONObject(Object[] objects, Map<Object, JSONObject> map) {
        JSONObject r = new JSONObject();

        for (Map.Entry<String, StructureRetour> e : this.retour.entrySet()) {

            e.getValue().initialiserJSONObject(e.getKey(), r, objects, map);
            if (retour.size() == 1) {
                return r.getJSONObject(e.getKey());
            }
        }
        return r;
    }

    public SelectDescriptionRetour(String source) throws NoSelect {
        int idx = 0;
        JPQLExpression jpql = new JPQLExpression(source, new JPQLGrammar2_1());

        List<Expression> ls = asList(jpql.getQueryStatement());
        if (!(ls.get(0) instanceof SelectClause)) {
            throw new NoSelect();
        }
        ls = asList(asList(ls.get(0)).get(2));
        for (Expression e : ls) {
            String s = e.toString();
            if (!s.equals(",") && !s.trim().isEmpty()) {
                List<Expression> selectElements = asList(e);
                if (selectElements.isEmpty()) {
                    ajouter(s, null, idx);
                    idx++;
                }
                if (selectElements.size() == 1) {

                    ajouter(s, null, idx);
                    idx++;
                }
                if (selectElements.size() == 2) {
                    String chemin[] = selectElements.get(1).toString().split("\\.");
                    ajouter(chemin, idx);
                    idx++;

                }
                if (selectElements.size() == 4) {
                    String chemin[] = selectElements.get(2).toString().split("\\.");
                    ajouter(selectElements.get(0).toString(), chemin, idx);
                    idx++;
                }

            }
        }
        ls = asList(asList(asList(jpql.getQueryStatement()).get(2)).get(2));
        for (Expression e : ls) {
            String s = e.toString();
            if (!s.equals(",") && !s.trim().isEmpty()) {
                List<Expression> tmpList = asList(e);
                List<Expression> fromElements = asList(tmpList.get(0));
                if (fromElements.size() != 3) {
                    fromElements = tmpList;
                }
                from.put(fromElements.get(2).toString(), fromElements.get(0).toString());

            }
        }

    }

    public void ajouter(String[] chemin, int idx) {
        StructureRetour sr = retour.get(chemin[0]);
        if (sr == null) {
            sr = new StructureRetour();
            this.retour.put(chemin[0], sr);
        }
        sr.ajouter(1, chemin, idx);
    }

    public void ajouter(String operation, String[] chemin, int idx) {
        StructureRetour sr = retour.get(operation);
        if (sr == null) {
            sr = new StructureRetour();
            this.retour.put(operation, sr);
        }
        if (chemin == null) {
            sr.idx = idx;
            return;
        }
        sr.ajouter(0, chemin, idx);
    }

    public List<Expression> asList(Expression expression) {
        List<Expression> lst = new ArrayList<>();
        for (Expression e : expression.orderedChildren()) {
            lst.add(e);
            //SuperLogger.println(e);

        }
        return lst;

    }

    public void creerTypeReponse(String nomService,ApiSignature apiSignature,Client client) {
        if (retour.size() == 1) {
            for (Map.Entry<String, StructureRetour> e : this.retour.entrySet()) {
                String typeFrom = from.get(e.getKey());
                if (typeFrom != null) {
                    apiSignature.typeReponse = typeFrom;
                    return ;
                }
            }
        }
        Type typeReponse = new Type();
        typeReponse.abstrait = false;
        typeReponse.nom = "Reponse" + nomService;
        apiSignature.typeReponse = typeReponse.nom;
        typeReponse.champs = new ArrayList<Champ>();
        for (Map.Entry<String, StructureRetour> e : this.retour.entrySet()) {
            Champ champ = new Champ();
            champ.nom = e.getKey();
            String typeEntity = this.from.get(champ.nom);
            if (typeEntity != null) {
                champ.type = typeEntity;
            } else {
                champ.type = "any";
            }
            typeReponse.champs.add(champ);
                
        }
        client.types.add(typeReponse);

       
    }
}
