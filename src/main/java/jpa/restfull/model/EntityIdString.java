package jpa.restfull.model;

import java.util.UUID;

public class EntityIdString extends EntityId {

	@Override
	public Class classId() {
		// TODO Auto-generated method stub
		if (this.generated) {
			return UUID.class;
		}
		return String.class;
	}
	public Object convertir(Object obj) {
		if (!generated) {
			return obj;
		}
		return UUID.fromString(obj.toString());
	}

    @Override
    public String nomType() {
        return "string";
    }

}
