package jpa.restfull.model.exception;

public class InvalidTypePourPersist extends Exception {
	public String type;
	public InvalidTypePourPersist(String type) {
		this.type = type;
	}
	public String toString() {
		return type;
	}

}
