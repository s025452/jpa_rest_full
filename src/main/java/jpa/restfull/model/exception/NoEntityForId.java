/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.exception;

/**
 *
 * @author david
 */
public class NoEntityForId extends Exception {
    public String type;
    public String id;
    public NoEntityForId(String type,String id) {
        this.type = type;
        this.id = id;
    }
    public String getMessage() {
        return type+" "+id;
    }
    
}
