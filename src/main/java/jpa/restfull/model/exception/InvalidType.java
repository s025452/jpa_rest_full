package jpa.restfull.model.exception;

public class InvalidType extends Exception {
	public String type;
	public InvalidType(String type) {
		this.type = type;
	}
	public String toString() {
		return type;
	}

}
