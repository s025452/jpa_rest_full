/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.exception;

import jpa.restfull.model.type.EntityFieldType;
import org.json.JSONObject;

/**
 *
 * @author DAVID
 */
public class ControlException extends Exception{
    JSONObject  jsonObject;
    String field;
    EntityFieldType type;

    public ControlException(JSONObject jsonObject, String field, EntityFieldType type) {
        this.jsonObject = jsonObject;
        this.field = field;
        this.type = type;
    }
    
    
}
