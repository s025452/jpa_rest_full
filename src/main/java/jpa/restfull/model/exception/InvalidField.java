package jpa.restfull.model.exception;

import org.json.JSONObject;

public class InvalidField extends Exception {
	JSONObject owner;
	String name;
	public InvalidField(String name,JSONObject jsonObject) {
		this.owner = jsonObject;
		this.name = name;
	}

}
