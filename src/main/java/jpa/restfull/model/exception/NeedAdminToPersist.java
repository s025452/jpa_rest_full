/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.exception;

/**
 *
 * @author david
 */
public class NeedAdminToPersist extends Exception{
   public String type;
   public  NeedAdminToPersist( String type) {
        this.type = type;
    }
    public String toString() {
		return type;
	}
    
}
