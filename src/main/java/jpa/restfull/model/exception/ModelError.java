package jpa.restfull.model.exception;

public class ModelError extends Exception {
	String type;
	String champ;
	String libelle;
	public ModelError(String type,String champ,String libelle) {
		this.type = type;
		this.champ = champ;
		this.libelle = libelle;
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("pour l'entite");
		sb.append(type);
		if (champ != null) {
			sb.append("et  le champ ");
			sb.append(champ);
		}
		sb.append(" : ");
		sb.append(libelle);
		return sb.toString();
	}

}
