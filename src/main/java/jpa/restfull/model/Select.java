package jpa.restfull.model;

import java.util.List;
import java.util.Map;

public class Select {
	public String query;
	public Map<String,Object> params;
	public List<List<String>> filter;
	public Integer pageSize;
	public int pageIndex;

}
