package jpa.restfull.model;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import jpa.restfull.model.api.NoSelect;
import jpa.restfull.model.api.SelectDescriptionRetour;
import jpa.restfull.model.json.convert.ContextEntityToJson;

import org.hibernate.query.internal.QueryImpl;

public class API {

    String source;
    APIDescription apiDescription;
    public SelectDescriptionRetour descriptionRetour;
    String messageErreur;
    public ContextEntityToJson.Mode mode;

    public API() {

    }

    public API(EntityManager entityManager, String source, ContextEntityToJson.Mode mode) {
        this.mode = mode;
        try {
            QueryImpl<?> q = (QueryImpl<?>) entityManager.createQuery(source);
            this.source = source;
            this.apiDescription = new APIDescription(q);

            this.descriptionRetour = new SelectDescriptionRetour(source);

        } catch (Exception e) {
            if (e.getClass() != NoSelect.class) {
                this.messageErreur = e.getMessage();
            }
        }

    }

    public String getSource() {
        return source;
    }

    public APIDescription getApiDescription() {
        return apiDescription;
    }

    public String getMessageErreur() {
        return messageErreur;
    }

    public ContextEntityToJson.Mode getMode() {
        return this.mode;
    }

    public void setMode(ContextEntityToJson.Mode mode) {
        this.mode = mode;
    }

}
