package jpa.restfull.model.json.object;

import java.util.List;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.BooleanMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import jpa.restfull.Constantes;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.type.EntityFieldTypeRef;

public class Index {
	public String nom;
	public Boolean unique;
	public List<Colonne> colonnes;
        public Annotation creerAnnotation(EntityDef entityDef,ConstPool constpool) {
            Annotation annotation = new Annotation("javax.persistence.Index", constpool);
            MemberValue mv = new StringMemberValue(nom, constpool);
            annotation.addMemberValue("name", mv);
            boolean first = true;
            StringBuilder sb = new StringBuilder();
            for(Colonne col:colonnes) {
                if (!first) {
                    sb.append(",");
                }
                first =false;
                sb.append(col.nom);
                if (entityDef.champs.get(col.nom) instanceof EntityFieldTypeRef) {
                    sb.append("_");
                    sb.append(Constantes.ID);
                }
            }
            mv = new StringMemberValue(sb.toString(), constpool);
            annotation.addMemberValue("columnList", mv);
            mv = new BooleanMemberValue(Boolean.TRUE.equals(unique), constpool);
            annotation.addMemberValue("unique", mv);
         
            return annotation;
        }

}
