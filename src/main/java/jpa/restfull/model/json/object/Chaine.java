package jpa.restfull.model.json.object;

import jpa.restfull.model.EntityId;
import jpa.restfull.model.EntityIdString;
import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeString;

public class Chaine extends Identifiant {

	@Override
	public EntityId convertir(boolean generer) {
		// TODO Auto-generated method stub
		EntityId r= new EntityIdString();
		r.setGenerated(generer);
		return r;
	}

	@Override
	public EntityFieldType type() {
		// TODO Auto-generated method stub
		return new EntityFieldTypeString(false);
	}
	

}
