/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.json.object.client;

import java.util.List;

/**
 *
 * @author DAVID
 */
public class Type {
    public Boolean abstrait;
    public String nom;
    public String superType;
    public  List<Champ> champs;
}
