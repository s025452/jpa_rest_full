package jpa.restfull.model.json.object;

import org.json.JSONObject;

import jpa.restfull.model.EntityId;
import jpa.restfull.model.type.EntityFieldType;

public abstract class Identifiant {
abstract	public EntityId convertir(boolean generer);
abstract public EntityFieldType type();

}
