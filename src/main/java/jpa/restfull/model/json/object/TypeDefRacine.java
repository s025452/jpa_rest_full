package jpa.restfull.model.json.object;

import jpa.restfull.Constantes;
import org.json.JSONObject;

import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.type.EntityFieldType;

public class TypeDefRacine extends TypeDef {
	public Identifiant typeClef;
	public Boolean genererClef;

	public void init(EntitiesDef entitiesDef) throws ModelError {
            
               
		super.init(null,entitiesDef);
        
                
		
	}
        public EntityDef creerEntityDef(String superType,EntitiesDef entitiesDef) throws ModelError {
        EntityDef entityDef = super.creerEntityDef(superType, entitiesDef);

		entityDef.entityId = typeClef.convertir(this.genererClef);
		entityDef.champs.put(Constantes.ID,typeClef.type());
                return entityDef;
        
    }

}
