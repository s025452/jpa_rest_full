package jpa.restfull.model.json.object;

import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeBoolean;

public class TypeBoolean extends TypePersistant {

	@Override
	public EntityFieldType convertir() {
		// TODO Auto-generated method stub
		return new EntityFieldTypeBoolean(Boolean.TRUE.equals(notNull));
	}

}
