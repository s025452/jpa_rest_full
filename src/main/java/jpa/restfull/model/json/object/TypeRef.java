package jpa.restfull.model.json.object;

import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeRef;

public class TypeRef extends TypePersistant {
	public String nom;
        public Boolean noInsert;
        public Boolean noUpdate;

	@Override
	public EntityFieldType convertir() {
		// TODO Auto-generated method stub
		return new EntityFieldTypeRef(nom,Boolean.TRUE.equals(noInsert),Boolean.TRUE.equals(noUpdate),Boolean.TRUE.equals(notNull));
	}
        
         public String controller(EntitiesDef entitiesDef) {
            if (entitiesDef.mapEntityDef.get(nom) == null) {
                return " le type "+nom+" n'existe pas";
            }
            return null;
        }

}
