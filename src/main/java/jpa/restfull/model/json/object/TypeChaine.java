package jpa.restfull.model.json.object;

import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeString;

public class TypeChaine extends TypePersistant {

	@Override
	public EntityFieldType convertir() {
		// TODO Auto-generated method stub
		return new EntityFieldTypeString(Boolean.TRUE.equals(notNull));
	}

}
