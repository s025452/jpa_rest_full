package jpa.restfull.model.json.object;

import java.util.List;
import java.util.Map;
import jpa.restfull.Constantes;

import org.json.JSONObject;

import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.IndexAvecType;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.object.client.Erreur;
import jpa.restfull.model.type.EntityFieldType;

public class TypeDef {

    public String nom;
    public Boolean abstrait;
    public Boolean persist;
    public List<Champ> champs;
    public List<Index> indexes;
    public List<TypeDef> sousTypes;

    public void initIndexes(EntitiesDef entitiesDef) {
        if (indexes != null) {
            for (Index index : indexes) {
                IndexAvecType indexAvecType = new IndexAvecType();
                indexAvecType.index = index;
                indexAvecType.type = nom;
                entitiesDef.indexes.put(nom + ":" + index.nom, indexAvecType);

            }
        }
    }

    public void controllerType(EntitiesDef entitiesDef, List<Erreur> erreurs) {
        for (Champ champ : champs) {
            String s = champ.type.controller(entitiesDef);
            if (s != null) {
                Erreur erreur = new Erreur();
                erreur.nom = "Type erreur";
                erreur.libelle = " erreur pour le type " + nom + " pour le champ " + champ.nom + " : " + s;
                erreurs.add(erreur);
            }
        }
        if (sousTypes != null) {
            for (TypeDef typeDef : this.sousTypes) {
                typeDef.controllerType(entitiesDef, erreurs);
            }
        }

    }

    public EntityDef creerEntityDef(String superType, EntitiesDef entitiesDef) throws ModelError {
        if (entitiesDef.mapEntityDef.get(nom) != null) {
            throw new ModelError(nom, null, " doublon ");
        }
        EntityDef entityDef = new EntityDef();
        entityDef.indexes = this.indexes;
        if (Boolean.TRUE.equals(persist)) {
            entityDef.persist = true;
        }
        for (Champ champ : this.champs) {
            EntityFieldType entityFieldType = champ.type.convertir();
            entityDef.champs.put(champ.nom, entityFieldType);
            if (entityFieldType.declareField()) {
                entityDef.declaredFields.add(champ.nom);
            }
            entityDef.allFields.add(champ.nom);
        }
        entitiesDef.mapEntityDef.put(this.nom, entityDef);
        entityDef.setAbstrait(abstrait);
        entityDef.setSuperEntityDef(superType);
        return entityDef;

    }

    public void init(String superType, EntitiesDef entitiesDef) throws ModelError {

        EntityDef entityDef = this.creerEntityDef(superType, entitiesDef);
        this.initIndexes(entitiesDef);
        if (superType != null) {
            EntityDef ed = entitiesDef.mapEntityDef.get(superType);
            for (Champ champ : this.champs) {
                if (ed.getChamps().get(champ.nom) != null) {
                    throw new ModelError(this.nom, champ.nom, " Champ présent dans " + nom);

                }
                if (Constantes.ID.equals(champ.nom)) {
                    throw new ModelError(nom, champ.nom, " Champ ( id ) reserver présent dans " + nom);
                }
            }
            entityDef.champs.putAll(ed.champs);

        }
        for (TypeDef typeDef : sousTypes) {
            typeDef.init(this.nom, entitiesDef);
        }

    }
}
