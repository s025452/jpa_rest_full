package jpa.restfull.model.json.object;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.IndexAvecType;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.object.client.Erreur;

public class Types {
	public List<TypeDefRacine> types = new ArrayList<>();
	
	public void init(EntitiesDef entitiesDef) throws ModelError {
		for(TypeDefRacine typeDefRacine:types) {
			typeDefRacine.init( entitiesDef);
                        
		}
		entitiesDef.init();
	}
        public void controllerType( EntitiesDef entitiesDef,List<Erreur> erreurs) {
            for(TypeDefRacine typeDefRacine:types) {
			typeDefRacine.controllerType(entitiesDef,erreurs);
		}
        }


}
