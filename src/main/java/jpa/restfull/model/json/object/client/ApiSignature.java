/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfull.model.json.object.client;

import java.util.List;
import jpa.restfull.model.api.SelectDescriptionRetour;

/**
 *
 * @author DAVID
 */
public class ApiSignature {
    public String typeQuestion;
    public Boolean estUnique;
    public String typeReponse;
    public SelectDescriptionRetour selectDescriptionRetour;
    
}
