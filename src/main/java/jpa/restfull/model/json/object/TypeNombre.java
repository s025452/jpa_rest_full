package jpa.restfull.model.json.object;

import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeNumber;

public class TypeNombre extends TypePersistant {
	@Override
	public EntityFieldType convertir() {
		return new EntityFieldTypeNumber(Boolean.TRUE.equals(notNull));
	}

}
