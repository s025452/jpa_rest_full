package jpa.restfull.model.json.object;

import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeListRelation;

public class TypeListRelation extends Type {
	public String nom;
	public String relation;
	@Override
	public EntityFieldType convertir() {
		return new EntityFieldTypeListRelation(nom,relation);
	}
}
