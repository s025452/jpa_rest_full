package jpa.restfull.model.json.object;

import jpa.restfull.model.EntityId;
import jpa.restfull.model.EntityIdLong;
import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeInteger;

public class Entier extends Identifiant {

	@Override
	public EntityId convertir(boolean generer) {
		EntityId r = new EntityIdLong();
		r.setGenerated(generer);
		return r;
	}

	@Override
	public EntityFieldType type() {
		// TODO Auto-generated method stub
		return new EntityFieldTypeInteger(false);
	}

}
