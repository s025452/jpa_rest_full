package jpa.restfull.model.json.object;

import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeInteger;

public class TypeEntier extends TypePersistant {

	@Override
	public EntityFieldType convertir() {
		return new EntityFieldTypeInteger(Boolean.TRUE.equals(notNull));
	}

}
