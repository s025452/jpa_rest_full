package jpa.restfull.model.json.object;

import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeList;

public class TypeList extends Type {
	public String nom;
	public String lienInverse;
	@Override
	public EntityFieldType convertir() {
		return new EntityFieldTypeList(nom, lienInverse);
	}

}
