package jpa.restfull.model.json.convert;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jpa.restfull.Constantes;
import jpa.restfull.model.json.object.Types;

public class ContextJavaToJson {

    MapCycleDetection map;
    public boolean useIdForCycle = false;
    Map<String, Map<String, CycleDetection>> references = new HashMap<>();

    public void getCycleDetection(Object obj, Field field)
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        Class type = field.getType();
        if (type == Boolean.class) {
            return;
        }
        if (type == Integer.class) {
            return;
        }
        if (type == String.class) {
            return;
        }
        if (type == List.class) {
            List list = (List) field.get(obj);
            for (Object elt : list) {
                if (elt.getClass() != String.class && elt.getClass() != Integer.class && elt.getClass() != Boolean.class) {
                    getCycleDetection(elt);
                }
            }
            return;
        }
        if (type == Map.class) {
            Map map = (Map) field.get(obj);
            for (Object elt : map.values()) {
                if (elt.getClass() != String.class && elt.getClass() != Integer.class && elt.getClass() != Boolean.class) {
                    getCycleDetection(elt);
                }
            }
            return;
        }
        getCycleDetection(field.get(obj));

    }

    public void getCycleDetection(Object obj)
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
        if (obj == null) {
            return;
        }
        CycleDetection cycleDetection = map.get(obj);
        if (cycleDetection == null) {
            cycleDetection = new CycleDetection();

            map.put(obj, cycleDetection);
        } else {
            cycleDetection.unique = false;
            return;
        }
        for (Field field : obj.getClass().getFields()) {
            getCycleDetection(obj, field);
        }

    }

    public JSONObject references() {
        JSONObject r = new JSONObject();
        for (Map.Entry<String, Map<String, CycleDetection>> e : this.references.entrySet()) {
            JSONObject tmp = new JSONObject();
            for (Map.Entry<String, CycleDetection> u : e.getValue().entrySet()) {
                if (!u.getValue().unique && !u.getValue().estRacine) {
                    u.getValue().object.remove(Constantes.ID);
                    JSONObject valeur = u.getValue().object.getJSONObject(Constantes.VALEUR);
                    valeur.remove(Constantes.ID);
                    tmp.put(u.getKey(), valeur);
                }
            }
            r.put(e.getKey(), tmp);
        }
        return r;
    }

    public JSONArray create(Object obj) throws IllegalArgumentException, IllegalAccessException, JSONException,
            NoSuchFieldException, SecurityException {
        this.map = new MapCycleDetection();
        this.getCycleDetection(obj);
        this.map.idx = 1;
        map.get(obj).estRacine = true;
        this.create(null, obj);
        JSONObject[] jsonObjects = new JSONObject[this.map.idx];
        for (CycleDetection cd : this.map.values()) {
            if (!cd.unique) {
                jsonObjects[cd.idx] = cd.object;
            }
        }
        jsonObjects[0] = map.get(obj).object;
        JSONArray r = new JSONArray();
        for (JSONObject o : jsonObjects) {
            r.put(o);
        }
        return r;

    }

    public boolean estAtome(Object elt) {
        if (elt.getClass() != String.class && elt.getClass() != Integer.class && elt.getClass() != Boolean.class) {
            return false;
        }
        return true;
    }

    public JSONArray create(List list) throws IllegalArgumentException, IllegalAccessException, JSONException,
            NoSuchFieldException, SecurityException {
        this.map = new MapCycleDetection();
        for (Object obj : list) {
            this.getCycleDetection(obj);
        }
        this.map.idx = 0;

        for (Object obj : list) {
            if (!estAtome(obj)) {
                map.get(obj).idx = this.map.idx;
                this.map.idx++;
                map.get(obj).estRacine = true;
            }
        }
        for (Object elt : list) {
            if (!estAtome(elt)) {
                this.create(null, elt);
            }
        }
        if (this.map.idx == 0) {
            JSONArray r = new JSONArray();

            for (Object elt : list) {
                r.put(elt);
            }
            return r;
        }
        JSONObject[] jsonObjects = new JSONObject[this.map.idx];
        for (CycleDetection cd : this.map.values()) {
            if (!cd.unique) {
                jsonObjects[cd.idx] = cd.object;
            }
        }
        JSONArray r = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            jsonObjects[i] = map.get(list.get(i)).object;
        }
        if (!this.useIdForCycle) {
            for (JSONObject o : jsonObjects) {
                r.put(o);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                r.put(jsonObjects[i]);
            }
        }
        return r;

    }

    public void customInit(JSONObject r, Object obj)
            throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {

    }

    public Object create(Class clsBase, Object obj) throws JSONException, IllegalArgumentException,
            IllegalAccessException, NoSuchFieldException, SecurityException {
        JSONObject r = null;

        CycleDetection cycleDetection = this.map.get(obj);
        if (cycleDetection == null) {
            throw new Error("ko null " + obj);
        }
        if (!cycleDetection.unique) {
            if (cycleDetection.object != null) {
                if (this.useIdForCycle) {
                    return createId(cycleDetection, clsBase, obj, cycleDetection.object);
                }

                return cycleDetection.idx;
            }
            r = new JSONObject();
            cycleDetection.object = r;
            if (!cycleDetection.estRacine) {
                cycleDetection.idx = map.idx;
                map.idx++;
            }

        } else {

            r = new JSONObject();
            cycleDetection.object = r;
        }
        for (Field field : obj.getClass().getFields()) {
            initField(field, obj, r);
        }
        this.customInit(r, obj);
        if (!cycleDetection.unique) {
            JSONObject tmp = new JSONObject();

            tmp.put(Constantes.TYPE, obj.getClass().getSimpleName());
            tmp.put(Constantes.VALEUR, r);
            cycleDetection.object = tmp;
            if (this.useIdForCycle) {

                return createId(cycleDetection, clsBase, obj, r);
            }
            return cycleDetection.idx;
        }
        if (addType(clsBase)) {
            JSONObject tmp = new JSONObject();

            tmp.put(Constantes.TYPE, obj.getClass().getSimpleName());
            tmp.put(Constantes.VALEUR, r);
            cycleDetection.object = tmp;
            r = tmp;
        }
        return r;

    }

    public Object createId(CycleDetection cycleDetection, Class clsBase, Object obj, JSONObject r) {
        Object id = r.opt(Constantes.ID);
        JSONObject valeur = r;
        if (id == null) {
            valeur = r.getJSONObject(Constantes.VALEUR);
            id = valeur.get(Constantes.ID);
        }
        JSONObject valeurId = new JSONObject();
        valeurId.put(Constantes.ID, id);
        String typeName = obj.getClass().getSimpleName();
        Map<String, CycleDetection> map = this.references.get(typeName);
        if (map == null) {
            map = new HashMap<>();
            this.references.put(typeName, map);

        }
        map.put(id.toString(), cycleDetection);
        if (!addType(clsBase)) {
            return valeurId;
        }
        JSONObject result = new JSONObject();
        result.put(Constantes.VALEUR, valeurId);
        result.put(Constantes.TYPE, typeName);

        return result;

    }

    public Object createSimple(Class clsBase, Object obj) throws JSONException, IllegalArgumentException,
            IllegalAccessException, NoSuchFieldException, SecurityException {
        JSONObject r = null;

        CycleDetection cycleDetection = this.map.get(obj);
        if (cycleDetection == null) {
            throw new Error("ko null");
        }
        if (!cycleDetection.unique) {
            if (cycleDetection.object != null) {
                return cycleDetection.idx;
            }
            r = new JSONObject();
            cycleDetection.object = r;
            if (!cycleDetection.estRacine) {
                cycleDetection.idx = map.idx;
                map.idx++;
            }

        } else {

            r = new JSONObject();
            cycleDetection.object = r;
        }
        for (Field field : obj.getClass().getFields()) {
            initField(field, obj, r);
        }
        this.customInit(r, obj);
        if (!cycleDetection.unique) {
            JSONObject tmp = new JSONObject();

            tmp.put(Constantes.TYPE, obj.getClass().getSimpleName());
            tmp.put(Constantes.VALEUR, r);
            cycleDetection.object = tmp;
            return cycleDetection.idx;
        }
        if (addType(clsBase)) {
            JSONObject tmp = new JSONObject();

            tmp.put(Constantes.TYPE, obj.getClass().getSimpleName());
            tmp.put(Constantes.VALEUR, r);
            cycleDetection.object = tmp;
            r = tmp;
        }
        return r;

    }

    public void initField(Field field, Object obj, JSONObject cible) throws JSONException, IllegalArgumentException,
            IllegalAccessException, NoSuchFieldException, SecurityException {
        Class type = field.getType();
        if (type == Boolean.class) {
            cible.put(field.getName(), field.get(obj));
            return;

        }
        if (type == String.class) {
            cible.put(field.getName(), field.get(obj));
            return;
        }
        if (type == Integer.class) {
            cible.put(field.getName(), field.get(obj));
            return;
        }
        if (type == List.class) {
            List list = (List) field.get(obj);
            ParameterizedType pt = (ParameterizedType) field.getGenericType();
            Class cls = (Class) pt.getActualTypeArguments()[0];
            boolean useCreate = (cls != String.class && cls != Boolean.class && cls != Integer.class);
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                if (!useCreate) {
                    jsonArray.put(list.get(i));
                } else {
                    jsonArray.put(create(cls, list.get(i)));
                }

            }
            cible.put(field.getName(), jsonArray);
            return;
        }
        if (type == Map.class) {
            Map map = (Map) field.get(obj);
            ParameterizedType pt = (ParameterizedType) field.getGenericType();
            if (pt.getActualTypeArguments()[0] != String.class) {
                return;
            }
            Class cls = (Class) pt.getActualTypeArguments()[1];
            boolean useCreate = (cls != String.class && cls != Boolean.class && cls != Integer.class);
            JSONObject object = new JSONObject();
            for (Object key : map.keySet()) {
                String s = (String) key;
                if (useCreate) {
                    object.put(s, create(cls, map.get(key)));
                } else {
                    object.put(s, map.get(key));
                }
            }
            cible.put(field.getName(), object);
            return;
        }
        Object fieldValue = field.get(obj);
        if (fieldValue == null) {
            return;
        }
        cible.put(field.getName(), create(type, fieldValue));

    }

    public boolean addType(Class type) {
        if (type == null) {
            return true;
        }
        return Modifier.isAbstract(type.getModifiers());
    }
}
