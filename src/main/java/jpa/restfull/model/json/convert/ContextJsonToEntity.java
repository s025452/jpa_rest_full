package jpa.restfull.model.json.convert;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.json.JSONException;
import org.json.JSONObject;

import jpa.restfull.Constantes;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.api.SelectDescriptionRetour;
import jpa.restfull.model.exception.NoEntityForId;
import jpa.restfull.model.type.EntityFieldType;

public class ContextJsonToEntity extends ContextJsonToJava {

    EntitiesDef entitiesDef;
    EntityManager entityManager;
    public Map<String, Map<String, Object>> references;

    public ContextJsonToEntity(EntitiesDef entitiesDef, EntityManager entityManager) {
        this.entitiesDef = entitiesDef;
        this.entityManager = entityManager;
    }

    public boolean needType(Class cls) {
        if (cls == null) {
            return true;
        }
        if (Modifier.isAbstract(cls.getModifiers())) {
            return true;
        }
        return entitiesDef.mapEntityDef.get(cls.getSimpleName()).isNeedType();
    }

    @Override
    public void init(JSONObject jsonObject, Object obj, String pckg, Field field)
            throws Exception {
        Class type = field.getType();
        if (type == UUID.class) {
            return;
        }
        String fieldName = field.getName();
        if (!Constantes.ID.equals(fieldName)) {
            EntityDef entityDef = this.entitiesDef.mapEntityDef.get(obj.getClass().getSimpleName());
            EntityFieldType typeField = entityDef.champs.get(fieldName);
            typeField.controlJSON(jsonObject, fieldName);
        }
        super.init(jsonObject, obj, pckg, field);

    }

    public Object createEmpty(JSONObject jsonObject, String pckg, Class cls)
            throws Exception {
        Object id = null;
        String type = null;
        if (needType(cls)) {
            type = jsonObject.getString(Constantes.TYPE);
            JSONObject jsObject = jsonObject.getJSONObject(Constantes.VALEUR);
            if (jsObject.has("id")) {
                id = jsObject.get("id");
            }

            cls = entitiesDef.mapEntityDef.get(type).cls;
        } else {
            if (jsonObject.has("id")) {
                id = jsonObject.get("id");
            }
        }
        if (type == null) {
            type = cls.getSimpleName();
        }
        if (id != null) {
            if (references != null) {
                if (references.get(type) != null) {
                    Object result = references.get(type).get(id.toString());
                    if (result != null) {
                        return result;
                    }

                }
               // return cls.getConstructor().newInstance();
            }
            id = entitiesDef.mapEntityDef.get(cls.getSimpleName()).entityId.convertir(id);
            Object r = entityManager.find(cls, id);
            if (r == null) {
                throw new NoEntityForId(type,id.toString());
            }
            return r;
        }
        return cls.getConstructor().newInstance();

    }

    public Object create(JSONObject jsonObject, String pckg, Class cls)
            throws Exception {
        Object r = createEmpty(jsonObject, pckg, cls);
        if (needType(cls)) {
            jsonObject = jsonObject.getJSONObject(Constantes.VALEUR);
        }
        init(jsonObject, r, pckg);
        return r;

    }
}
