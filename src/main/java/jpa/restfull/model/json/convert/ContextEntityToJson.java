package jpa.restfull.model.json.convert;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jpa.restfull.Constantes;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.api.SelectDescriptionRetour;
import jpa.restfull.model.type.EntityFieldType;
import jpa.restfull.model.type.EntityFieldTypeList;
import jpa.restfull.model.type.EntityFieldTypeListRelation;

public class ContextEntityToJson extends ContextJavaToJson {
	EntitiesDef entitiesDef;
	EntityManager entityManager;
     

	public enum Mode { Deep , Simple , NoID }
        
        Mode mode = Mode.Deep;
	Map<Object, Map<String, List>> relations;

	public ContextEntityToJson(EntitiesDef entitiesDef, EntityManager entityManager) {
		this.entitiesDef = entitiesDef;
		this.entityManager = entityManager;
		this.relations = new HashMap<>();
	
			
	}
	public ContextEntityToJson(EntitiesDef entitiesDef, EntityManager entityManager,Mode mode) {
		this.entitiesDef = entitiesDef;
		this.entityManager = entityManager;
		this.relations = new HashMap<>();
                this.mode = mode;
	
			
	}
        
	public boolean addType(Class type) {
		if (type == null) {
			return true;
		}
		if (Modifier.isAbstract(type.getModifiers())) {
			return true;
		}
		return entitiesDef.mapEntityDef.get(type.getSimpleName()).isNeedType();
	}

	public boolean addType(String type) {
		if (type == null) {
			return true;
		}
		EntityDef entityDef = entitiesDef.mapEntityDef.get(type);
		if (entityDef.abstrait) {
			return true;
		}
		for (EntityDef e : entitiesDef.mapEntityDef.values()) {
			if (type.equals(e.superEntityDef)) {
				return true;
			}
		}
		return false;
	}

	public void initField(Field field, Object obj, JSONObject cible) throws JSONException, IllegalArgumentException,
			IllegalAccessException, NoSuchFieldException, SecurityException {
		if (field.getType() == UUID.class) {
                     if (mode == Mode.NoID) {
                            if (field.getName().equals(Constantes.ID)) {
                                return;
                            }
                        }
			cible.put(field.getName(), field.get(obj).toString());
			return;
		}
		if (mode != Mode.Simple) {
                        if (mode == Mode.NoID) {
                            if (field.getName().equals(Constantes.ID)) {
                                return;
                            }
                        }
			super.initField(field, obj, cible);
			return;
		}
		Object fieldValue = field.get(obj);
		if (fieldValue == null) {
			super.initField(field, obj, cible);
			return;
		}
		JSONObject id = createId(field.getType(), fieldValue);
		if (id != null) {
			cible.put(field.getName(), id);
			return;
		}
		super.initField(field, obj, cible);

	}

	public JSONObject createId(Class cls, Object obj) throws JSONException, IllegalArgumentException,
			IllegalAccessException, NoSuchFieldException, SecurityException {
		String typeName = obj.getClass().getSimpleName();
		if (entitiesDef.mapEntityDef.get(typeName) != null) {

			JSONObject jsonObject = new JSONObject();
			JSONObject id = new JSONObject();
			id.put(Constantes.ID, obj.getClass().getField(Constantes.ID).get(obj));
			if (addType(cls)) {
				jsonObject.put(Constantes.TYPE, typeName);
				jsonObject.put(Constantes.VALEUR, id);
				return jsonObject;
			}
			return id;
		}
		return null;
	}

	public void getCycleDetection(Object obj)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		super.getCycleDetection(obj);
		if (obj == null) {
			return;
		}
		EntityDef entityDef = this.entitiesDef.mapEntityDef.get(obj.getClass().getSimpleName());
		if (entityDef == null) {
			return;
		}

		if (relations.get(obj) != null) {
			return;
		}
		Map<String, List> map = new HashMap<>();
		relations.put(obj, map);
		for (Map.Entry<String, EntityFieldType> e : entityDef.champs.entrySet()) {
			EntityFieldType entityFieldType = e.getValue();
			String nom = e.getKey();
			if (entityFieldType.getClass() == EntityFieldTypeList.class) {
				EntityFieldTypeList entityFieldTypeList = (EntityFieldTypeList) entityFieldType;
				Query query = entityManager.createQuery("Select e from " + entityFieldTypeList.nom + " e where e."
						+ entityFieldTypeList.lienInverse + "= :obj");
				query.setParameter("obj", obj);
				List list = query.getResultList();
				map.put(nom, list);
				for (Object elt : list) {
					this.getCycleDetection(elt);
				}
			}
			if (entityFieldType.getClass() == EntityFieldTypeListRelation.class) {

				EntityFieldTypeListRelation entityFieldTypeList = (EntityFieldTypeListRelation) entityFieldType;
				Query query = entityManager.createQuery("Select e." + entityFieldTypeList.colonneDestination + " from "
						+ entityFieldTypeList.relation + " e where e." + entityFieldTypeList.colonneSource + "= :obj");
				query.setParameter("obj", obj);
				List list = query.getResultList();
				map.put(nom, list);
				for (Object elt : list) {
					this.getCycleDetection(elt);
				}
			}
		}

	}

	public void customInit(JSONObject r, Object obj)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
               
		EntityDef entityDef = this.entitiesDef.mapEntityDef.get(obj.getClass().getSimpleName());
             
		for (Map.Entry<String, EntityFieldType> e : entityDef.champs.entrySet()) {
			EntityFieldType entityFieldType = e.getValue();
			String nom = e.getKey();
			if (entityFieldType.getClass() == EntityFieldTypeList.class) {
				EntityFieldTypeList entityFieldTypeList = (EntityFieldTypeList) entityFieldType;

				r.put(nom, transformList(nom,entityFieldTypeList.nom, obj));
			}
			if (entityFieldType.getClass() == EntityFieldTypeListRelation.class) {
				EntityFieldTypeListRelation entityFieldTypeList = (EntityFieldTypeListRelation) entityFieldType;
				r.put(nom, transformList(nom,entityFieldTypeList.nom, obj));
			}

		}

	}

	private JSONArray transformList(String nom,String typeElement, Object obj) throws JSONException, IllegalArgumentException,
			IllegalAccessException, NoSuchFieldException, SecurityException {
		JSONArray jsonArray = new JSONArray();
		List list = this.relations.get(obj).get(nom);
		for (Object elt : list) {
			if (mode != Mode.Simple) {
				jsonArray.put(this.create(entitiesDef.mapEntityDef.get(typeElement).cls, elt));
			} else {

				jsonArray.put(this.createId(entitiesDef.mapEntityDef.get(typeElement).cls, elt));
			}

		}
		return jsonArray;
	}
}
