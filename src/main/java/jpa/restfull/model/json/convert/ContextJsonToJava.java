package jpa.restfull.model.json.convert;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jpa.restfull.Constantes;
import jpa.restfull.model.json.object.TypeRef;

public class ContextJsonToJava {
		JSONArray references;
	Map<Integer, Object> resolutions;
 

	public List emptyList() {
		return new ArrayList();
	}
        public Map emptyMap() {
            return new HashMap();
        }

	public void init(JSONObject jsonObject, Object obj, String pckg, Field field)
			throws Exception {
		Class type = field.getType();
		if (!jsonObject.has(field.getName())) {
			return;
		}
		if (type == String.class) {
			String v = jsonObject.getString(field.getName());
			field.set(obj, v);
			return;
		}
		if (type == Boolean.class) {
			Object v = jsonObject.get(field.getName());
			if (v instanceof Boolean) {
				field.set(obj, v);
			} else {
				throw new Error(field.getName());
			}
			return;
		}
		if (type == Integer.class) {
			Integer v = jsonObject.getInt(field.getName());
			if (v instanceof Integer) {
				field.set(obj, v);
			} else {
				throw new Error(field.getName());
			}
			return;
		}
		if (type == List.class) {
			ParameterizedType pt = (ParameterizedType) field.getGenericType();
			Class cls = (Class) pt.getActualTypeArguments()[0];
			List list = this.emptyList();
			JSONArray jsonArray = jsonObject.getJSONArray(field.getName());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject tmp = jsonArray.optJSONObject(i);
				if (tmp == null) {
                                    if (cls == String.class) {
                                        list.add(jsonArray.getString(i));
                                    } else if (cls == Integer.class) {
                                        list.add(jsonArray.getInt(i));
                                    } else {
					int idx = jsonArray.getInt(i);
					list.add(getObjectForIdx(idx,pckg)); 
                                    }

				} else {

					list.add(create(tmp, pckg, cls));
				}
			}
			field.set(obj, list);
			return;

		}
                if (type == Map.class) {
                    	ParameterizedType pt = (ParameterizedType) field.getGenericType();
			if ( pt.getActualTypeArguments()[0] != String.class ) {
                            return ;
                        }
                        Class cls = (Class) pt.getActualTypeArguments()[1];
                        Map map = this.emptyMap();
                        JSONObject jsonObjectToMap  =  jsonObject.getJSONObject(field.getName());
                        for(String key:jsonObjectToMap.keySet()) {
                                JSONObject tmp = jsonObjectToMap.optJSONObject(key);
                                if (tmp != null) {
                                    map.put(key, create(tmp, pckg, cls));
                                } else {
                                    if (cls == String.class) {
                                        map.put(key, jsonObjectToMap.getString(key));
                                    } else if (cls == Integer.class) {
                                        map.put(key, jsonObjectToMap.getInt(key));                                        
                                    } else {
                                    int idx = jsonObjectToMap.getInt(key);
					map.put(key,getObjectForIdx(idx,pckg));
                                    }
                                }
                        }
                        field.set(obj, map);
		return;
                }

		JSONObject tmp = jsonObject.optJSONObject(field.getName());
		if (tmp == null) {
			int idx = jsonObject.getInt(field.getName());
			
			field.set(obj, getObjectForIdx(idx,pckg));

			return;
		}
		field.set(obj, create(tmp, pckg, field.getType()));

	}
	public Object getObjectForIdx(int idx,String pckg) throws Exception{
		Object emptyObject = resolutions.get(idx);
		if (emptyObject == null) {
			JSONObject jsonObject =  this.references.getJSONObject(idx);
			emptyObject = createEmpty(jsonObject, pckg, Class.forName(pckg+"."+jsonObject.getString(Constantes.TYPE)));
			resolutions.put(idx, emptyObject);
			init(jsonObject.getJSONObject(Constantes.VALEUR), emptyObject, pckg);
		}
		return emptyObject;
	}
	public void init(JSONObject jsonObject, Object obj, String pckg)
			throws Exception {
		Field fields[] = obj.getClass().getFields();
		for (Field field : fields) {
			init(jsonObject, obj, pckg, field);

		}
	}

	public Object createEmpty(JSONObject jsonObject, String pckg, Class cls)
			throws Exception {

		if (needType(cls)) {
			String type = jsonObject.getString(Constantes.TYPE);
			JSONObject jsObject = jsonObject.getJSONObject(Constantes.VALEUR);
			cls = Class.forName(pckg + "." + type);
		}
		return cls.getConstructor().newInstance();

	}

	public Object create(JSONArray array, String pckg)
			throws Exception {
		this.references = array;
		this.resolutions = new HashMap<>();
		JSONObject first = array.getJSONObject(0);

		String type = first.getString(Constantes.TYPE);
		JSONObject valeur = first.getJSONObject(Constantes.VALEUR);
		Object o = this.createEmpty(valeur, pckg, Class.forName(pckg + "." + type));
		this.resolutions.put(0, o);
		init(valeur, o, pckg); 
		return o;
	}

	public Object create(JSONObject jsonObject, String pckg, Class cls)
			throws Exception {
		Object r = createEmpty(jsonObject, pckg, cls);
		if (needType(cls)) {
			jsonObject = jsonObject.getJSONObject(Constantes.VALEUR);
		}
		init(jsonObject, r, pckg);
		return r;

	}


	public boolean needType(Class cls) {
		return Modifier.isAbstract(cls.getModifiers());
	}
}
