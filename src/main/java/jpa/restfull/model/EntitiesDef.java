package jpa.restfull.model;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import jpa.restfull.EntityTree;
import jpa.restfull.model.exception.InvalidField;
import jpa.restfull.model.exception.InvalidType;
import jpa.restfull.model.exception.InvalidTypePourPersist;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.type.EntityFieldType;

public class EntitiesDef {

    public Map<String, EntityDef> mapEntityDef = new HashMap<>();
    public static String TYPE = "type";
    public static String VALUES = "values";
    public Map<String,IndexAvecType> indexes = new HashMap<>();
    public EntityTree entityTree;
    public EntitiesDef(Types types) throws ModelError {
        types.init(this);

    }

    public boolean persist(String entity) throws InvalidTypePourPersist {
        EntityDef entityDef = mapEntityDef.get(entity);
        if (entityDef == null) {
           
            throw new InvalidTypePourPersist(entity);
           
        }
        if (entityDef.persist == null) {
            if (entityDef.superEntityDef == null) { 
                return false;
            }
            return persist(entityDef.superEntityDef);
        }
        return entityDef.persist;

    }

    public void recupererClientType(List<jpa.restfull.model.json.object.client.Type> types) {
        for (Map.Entry<String, EntityDef> e : mapEntityDef.entrySet()) {
            types.add(e.getValue().clientType(e.getKey()));
        }
    }

    static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
    ClassPool classPool;
    public void generate(String pckg,  ClassPool classPool)
            throws CannotCompileException, NotFoundException, IOException, URISyntaxException {
       // deleteDirectory(new File(path));
        this.classPool = classPool;
        this.entityTree = new EntityTree();
       // classPool.appendClassPath(path);
        
        for (Map.Entry<String, EntityDef> e : this.mapEntityDef.entrySet()) {
            EntityDef entityDef = e.getValue();

            String className = pckg + "." + e.getKey();
            entityDef.ctClass = classPool.makeClass(className);
            //entityDef.cls = entityDef.ctClass.toClass();

        }
        for (Map.Entry<String, EntityDef> e : this.mapEntityDef.entrySet()) {
            EntityDef entityDef = e.getValue();

            entityDef.generate(this, e.getKey(), classPool, pckg);

        }
        for (Map.Entry<String, EntityDef> e : this.mapEntityDef.entrySet()) {
            EntityDef entityDef = e.getValue();

         //  entityDef.ctClass.writeFile(path);
           //1 entityDef.ctClass.w
            //	entityDef.cls = entityDef.ctClass.toClass();

        }
        for (Map.Entry<String, EntityDef> e : this.mapEntityDef.entrySet()) {
            EntityDef entityDef = e.getValue();
            entityDef.buildClass(this);

        }
        for (Map.Entry<String, EntityDef> e : this.mapEntityDef.entrySet()) {
            EntityDef entityDef = e.getValue();
            entityDef.initFields();

        }
    }
    public void addEntity(String name,byte bytes[]) {
        String array[]= name.split(("\\."));
        EntityTree et = this.entityTree;
        for(int idx=0;idx < array.length;idx++) {
            String nom = array[idx];
            if (et.children.get(nom) == null) {
                EntityTree n = new EntityTree();
                et.children.put(nom, n);
                et = n;
                
            } else {
                et = et.children.get(nom);
            } 
        }
        et.bytes = bytes;
        
    }

    public void subEntityDef(String name, Map<String, EntityDef> r) {
        EntityDef ed = this.mapEntityDef.get(name);
        if (!ed.isAbstrait()) {
            r.put(name, ed);
        }
        for (Map.Entry<String, EntityDef> e : r.entrySet()) {
            String superEntityDef = e.getValue().superEntityDef;
            if (name.equals(superEntityDef)) {
                subEntityDef(superEntityDef, r);
            }
        }
    }

    public Map<String, EntityDef> subEntityDef(String name) {
        Map<String, EntityDef> r = new HashMap<>();
        subEntityDef(name, r);
        return r;
    }

    

    public void init() {
        for (Map.Entry<String, EntityDef> e : mapEntityDef.entrySet()) {
            e.getValue().setNeedType(needType(e.getKey()));
            for (Map.Entry<String, EntityFieldType> e2 : e.getValue().champs.entrySet()) {
                e2.getValue().init(this, e.getKey());
            }
        }
    }

    public boolean needType(String nom) {
        for (EntityDef e : mapEntityDef.values()) {
            if (nom.equals(e.superEntityDef)) {
                return true;
            }
        }
        return false;
    }
}
