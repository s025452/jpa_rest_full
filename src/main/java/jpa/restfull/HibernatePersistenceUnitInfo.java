package jpa.restfull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

public class HibernatePersistenceUnitInfo implements PersistenceUnitInfo {
	  public static String JPA_VERSION = "2.1";
	    private String persistenceUnitName;
	    private PersistenceUnitTransactionType transactionType
	      = PersistenceUnitTransactionType.RESOURCE_LOCAL;
	    public static List<URL> JarFileUrls = new ArrayList<>();
	   public static  List<String> managedClassNames = new ArrayList<>();
	    private List<String> mappingFileNames = new ArrayList<>();
	    private Properties properties;
	    private DataSource jtaDataSource;
	    private DataSource nonjtaDataSource;
	    private List<ClassTransformer> transformers = new ArrayList<>();
	    private ClassLoader classLoader;	
	    public HibernatePersistenceUnitInfo(
	      String persistenceUnitName, Properties properties,ClassLoader classLoader) {
	        this.persistenceUnitName = persistenceUnitName;
	        
	        this.properties = properties;

	        this.classLoader = classLoader;
	    }
	@Override
	public String getPersistenceUnitName() {
		// TODO Auto-generated method stub
		return persistenceUnitName;
	}

	@Override
	public String getPersistenceProviderClassName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PersistenceUnitTransactionType getTransactionType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataSource getJtaDataSource() {
		// TODO Auto-generated method stub
		return jtaDataSource;
	}

	@Override
	public DataSource getNonJtaDataSource() {
		// TODO Auto-generated method stub
		return nonjtaDataSource;
	}

	@Override
	public List<String> getMappingFileNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<URL> getJarFileUrls() {
		// TODO Auto-generated method stub
		return JarFileUrls;
	}

	@Override
	public URL getPersistenceUnitRootUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getManagedClassNames() {
		// TODO Auto-generated method stub
		return managedClassNames;
	}

	@Override
	public boolean excludeUnlistedClasses() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SharedCacheMode getSharedCacheMode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidationMode getValidationMode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Properties getProperties() {
		// TODO Auto-generated method stub
		return properties;
	}

	@Override
	public String getPersistenceXMLSchemaVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ClassLoader getClassLoader() {
		// TODO Auto-generated method stub
		return new CustomClassLoader();
	}

	@Override
	public void addTransformer(ClassTransformer transformer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ClassLoader getNewTempClassLoader() {
		// TODO Auto-generated method stub
		return new CustomClassLoader();
	}

}
