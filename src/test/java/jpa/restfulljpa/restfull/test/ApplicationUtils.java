/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.UnsupportedEncodingException;
import jpa.restfull.Application;
import jpa.restfull.Constantes;
import jpa.restfull.model.APICall;
import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.client.ResultatInstallation;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author david
 */
public class ApplicationUtils {

    static public ResultatInstallation install(String url, Install install) throws Exception {
        ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();

        JSONObject jsonObjectInstall = contextJavaToJson.create(install).getJSONObject(0).getJSONObject(Constantes.VALEUR);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(jsonObjectInstall.toString()));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(httpPost)) {
            String pckg = "jpa.restfull.model.json.object.client";

            String resultat = EntityUtils.toString(response.getEntity());
            JSONArray array = new JSONArray(resultat);
            return (ResultatInstallation) contextJsonToJava.create(array.getJSONObject(0), pckg, ResultatInstallation.class);

        }

    }

    static public String persist(String url, JSONObject object) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(object.toString()));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(httpPost)) {

            return EntityUtils.toString(response.getEntity());

        }


    }

    static public JSONObject api(String url, APICall apic ) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(objectMapper.writeValueAsString(apic)));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(httpPost)) {

            return new JSONObject(EntityUtils.toString(response.getEntity()));

        }
    }
       static public JSONObject apiUnique(String url, APICall apic ) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(objectMapper.writeValueAsString(apic)));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(httpPost)) {
            if (response.getStatusLine().getStatusCode() == 404) {
                return null;
            }

            return new JSONObject(EntityUtils.toString(response.getEntity()));

        }
    }
}
