package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;


import org.hibernate.engine.jdbc.StreamUtils;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;

public class TestCreationTypes {
	
	@org.junit.Test
	public void test() throws Exception {
		
		JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
		ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
		
		Object obj =contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
		
		ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
		System.out.println( contextJavaToJson.create(obj));

	}

}
