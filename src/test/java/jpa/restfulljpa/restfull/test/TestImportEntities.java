/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import jpa.restfull.Constantes;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.JsonTool;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.API;
import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;

/**
 *
 * @author david
 */
public class TestImportEntities {

    @org.junit.Test()
    public void testSelectDeuxObjetsAvecJointure() throws Exception {

        SuperLogger.println("Debut test");

        try {
            JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");

            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
            Types types = model.model;
            EntitiesDef entitiesDef = new EntitiesDef(types);
            ClassPool classPool = new ClassPool();

            classPool.appendSystemPath();
            String pckg = "model.jpa.testpersistentities";
            entitiesDef.generate(pckg, classPool);

            HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();

            EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
            JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
            JpaEntityManagerFactory.auto = "create";
            EntityManager em = factory.getEntityManager();
            //Object o = cls.getConstructors()[0].newInstance();
            List<APIRest> list = model.apis;

            APIDef apiDef = new APIDef(entitiesDef, list, em);
            Engine engine = new Engine(entitiesDef, apiDef, pckg);

            APICall apiCall = new APICall();
            apiCall.type = "voitureEtMaisonPourMemeProprietaire";
            apiCall.valeur = new HashMap<>();
            // apiCall.valeur.put("valeur", 0);
            API api = apiDef.recupererAPI(apiCall.type);
            api.mode = ContextEntityToJson.Mode.Deep;

            Assert.assertEquals(api.getMessageErreur(), null);

            SuperLogger.println(api.getApiDescription().getRetour());
            Assert.assertNotNull(api);
            em.getTransaction().begin();
            JSONObject importEntities = JsonTool.loadJSONObject("/model/import.json");
            //em.persist(o);
            try {
                SuperLogger.println(engine.persistEntities(em, importEntities, true));
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            em.getTransaction().commit();
            JSONObject resultat = (JSONObject) engine.execute(em, apiCall);
            JSONArray jsonArray = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertTrue(jsonArray.length() == 3);
            Assert.assertTrue(resultat.getJSONObject(Constantes.REFERENCES).getJSONObject("Personne").length() == 3);
            SuperLogger.println(resultat.toString());
            importEntities = JsonTool.loadJSONObject("/model/import.json");

            //em.persist(o);
            /*   em.getTransaction().begin();
            try {
                SuperLogger.println(engine.persistEntities(em, importEntities, true));
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            em.getTransaction().commit();
            resultat = (JSONObject) engine.execute(em, apiCall);
             jsonArray = resultat.getJSONArray(Constantes.VALEUR);
             int n = jsonArray.length();
            Assert.assertTrue(n == 6);
            Assert.assertTrue(resultat.getJSONObject(Constantes.REFERENCES).getJSONObject("Personne").length() == 6);
             */
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

}
