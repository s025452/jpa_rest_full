package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import jpa.restfull.Constantes;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.Select;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;

public class TestChargerSchemaTestLienInverse {

	
	@Test
	public void test() throws JsonMappingException, JsonProcessingException, IOException, ModelError, CannotCompileException, NotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, URISyntaxException, NoSuchFieldException, NoSuchMethodException, Exception {
		
		JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
		ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
		Model model = (Model)contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
		Types types =model.model;
		EntitiesDef entitiesDef = new EntitiesDef(types);
		Assert.assertTrue(entitiesDef.mapEntityDef.size() == 4);
		EntityDef entityDef = entitiesDef.mapEntityDef.get("Personne");
		Assert.assertTrue( entityDef.champs.size() == 5);
		Assert.assertTrue(entityDef != null );
		Assert.assertTrue(entityDef.entityId != null);
		Assert.assertTrue(entityDef.entityId.isGenerated());
		
		ClassPool classPool =new ClassPool();
		
		String cheminClasses ="c:/tmp/classes/";
		
	classPool.appendSystemPath();
//		classPool.
		entitiesDef.generate("model.jpa", classPool);
	
//	Class clsTest =urlClassLoader.loadClass(jpa.restfulljpa.restfull.test.Test.class.getName());
//	clsTest.getConstructor(String.class).newInstance(cheminClasses);
	           EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
           JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
		JpaEntityManagerFactory.auto = "create";
		EntityManager em = factory.getEntityManager();
		//Object o = cls.getConstructors()[0].newInstance();
		Engine engine = new Engine(entitiesDef,null,"model.jpa");
		em.getTransaction().begin();
		//em.persist(o);
		JSONObject entreprise = JsonTool.loadJSONObject("/e.json");
		Object idE = engine.persist(em, entreprise,true);
		entreprise.getJSONObject(Constantes.VALEUR).put("id",idE.toString());
		em.getTransaction().commit();
		JSONObject personne0 =	JsonTool.loadJSONObject("/p0.json");
		JSONObject personne1 =	JsonTool.loadJSONObject("/p1.json");
		personne0.getJSONObject(Constantes.VALEUR).put("entreprise",entreprise.getJSONObject(Constantes.VALEUR));
		personne1.getJSONObject(Constantes.VALEUR).put("entreprise", entreprise.getJSONObject(Constantes.VALEUR));
		em.getTransaction().begin();
		//em.persist(o);
                try  {
	 engine.persist(em, personne0,true); } catch(Exception e) {
             e.printStackTrace();
             throw e;
         }
	 engine.persist(em, personne1,true);
		em.getTransaction().commit();
		
		Select select  = new Select();
		select.query = "Select p from Personne p";
		JSONArray jsonArray = engine.select(em, select,ContextEntityToJson.Mode.Deep);
		Assert.assertTrue( jsonArray.length() == 3);

		select.query = "Select p from Personne p";
		jsonArray = engine.select(em, select,ContextEntityToJson.Mode.Simple);
		Assert.assertTrue( jsonArray.length() == 2);
		for(int idx=0;idx < jsonArray.length() ;idx++) {
			Assert.assertTrue(jsonArray.getJSONObject(idx).getJSONObject(Constantes.VALEUR).getJSONObject("entreprise").length() == 1) ;
		}
		System.out.println(jsonArray);
		select.query = "Select p from Entreprise p";
		jsonArray = engine.select(em, select,ContextEntityToJson.Mode.Deep);
		//Assert.assertTrue( jsonArray.length() == 1);
		
		Assert.assertTrue( jsonArray.length() == 1);
		Assert.assertTrue(jsonArray.getJSONObject(0).getJSONObject(Constantes.VALEUR).getJSONArray("salaries").length() == 2);
		JSONArray jsArray = jsonArray.getJSONObject(0).getJSONObject(Constantes.VALEUR).getJSONArray("salaries");
		for(int i=0;i < jsArray.length();i++) {
			Assert.assertTrue(jsArray.getJSONObject(i).length() > 1);
		}
		jsonArray = engine.select(em, select,ContextEntityToJson.Mode.Simple);
		
		Assert.assertTrue(jsonArray.getJSONObject(0).getJSONObject(Constantes.VALEUR).getJSONArray("salaries").length() == 2);
		 jsArray = jsonArray.getJSONObject(0).getJSONObject(Constantes.VALEUR).getJSONArray("salaries");
		System.out.println( jsArray);
		for(int i=0;i < jsArray.length();i++) {
			Assert.assertTrue(jsArray.getJSONObject(i).length() == 1);
		}
	}
	
	
}
