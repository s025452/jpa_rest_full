/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import jpa.restfull.Constantes;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.*;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

/**
 *
 * @author DAVID
 */
public class TestChargerSchemaTestAPIDelete {

    @org.junit.Test
    public void test2() throws Exception {
        SuperLogger.println("Debut test");
        JSONObject jsonObject = JsonTool.loadJSONObject("/test3.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        ClassPool classPool = new ClassPool();

        String cheminClasses = "c:/tmp/classes/" + this.getClass().getSimpleName() + "/";

        classPool.appendSystemPath();
        String pckg = "model10.jpa.test";
        entitiesDef.generate(pckg, classPool);

        EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
        JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
        JpaEntityManagerFactory.auto = "create";
        EntityManager em = factory.getEntityManager();
        //Object o = cls.getConstructors()[0].newInstance();
        List<APIRest> list = model.apis;

        APIDef apiDef = new APIDef(entitiesDef, list, em);
        List<jpa.restfull.model.json.object.client.Type> ls = new ArrayList<>();

        apiDef.recupererClientType(ls);
        Engine engine = new Engine(entitiesDef, apiDef, pckg);
        JSONObject personne = JsonTool.loadJSONObject("/p0_1.json");
        em.getTransaction().begin();
        engine.persist(em, personne, true);
        em.getTransaction().commit();
        APICall apiCall = new APICall();
        apiCall.type = "personnes";
        JSONObject resultat = (JSONObject) engine.execute(em, apiCall);
        JSONArray r = resultat.getJSONArray(Constantes.VALEUR);
        Assert.assertEquals(r.length(), 1);

        apiCall.type = "supprimer";
        apiCall.valeur.put("nom", "Toto");
        apiCall.valeur.put("prenom", "Titi");
        engine.execute(em, apiCall);
        apiCall = new APICall();
        apiCall.type = "personnes";
        apiCall.valeur.clear();
        resultat = (JSONObject) engine.execute(em, apiCall);
        r = resultat.getJSONArray(Constantes.VALEUR);
        Assert.assertEquals(r.length(), 0);

    }
}
