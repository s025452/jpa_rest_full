/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import jpa.restfull.Constantes;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.JsonTool;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.API;
import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;

/**
 *
 * @author david
 */
public class TestSelectColonnes {


    @org.junit.Test()
    public void test() {

        SuperLogger.println("Debut test");

        try {
            JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");

            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
            Types types = model.model;
            EntitiesDef entitiesDef = new EntitiesDef(types);
            ClassPool classPool = new ClassPool();

            classPool.appendSystemPath();
            String pckg = "model.jpa.testcol";
            entitiesDef.generate(pckg, classPool);

            HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();

            EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
            JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
            JpaEntityManagerFactory.auto = "create";
            EntityManager em = factory.getEntityManager();
            //Object o = cls.getConstructors()[0].newInstance();
            List<APIRest> list = model.apis;

            APIDef apiDef = new APIDef(entitiesDef, list, em);
            Engine engine = new Engine(entitiesDef, apiDef, pckg);

            JSONObject personne = JsonTool.loadJSONObject("/p0.json");
            APICall apiCall = new APICall();
            apiCall.type = "recupererPersonnes";
            apiCall.valeur = new HashMap<>();
            apiCall.valeur.put("valeur", 0);
            API api = apiDef.recupererAPI(apiCall.type);
            Assert.assertEquals(api.getMessageErreur(), null);

            Assert.assertEquals(api.getApiDescription().getParameters().get("valeur"), "number");

            SuperLogger.println(api.getApiDescription().getRetour());
            Assert.assertNotNull(api);
            em.getTransaction().begin();
            //em.persist(o);
            try {
                SuperLogger.println(engine.persist(em, personne, true));
                SuperLogger.println(engine.persist(em, JsonTool.loadJSONObject("/p1.json"), true));
                SuperLogger.println(engine.persist(em, JsonTool.loadJSONObject("/p2.json"), true));
                SuperLogger.println(engine.persist(em, JsonTool.loadJSONObject("/p4.json"), true));
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            em.getTransaction().commit();
            JSONObject r = (JSONObject) engine.execute(em, apiCall);
            SuperLogger.println(r.toString());
            apiCall = new APICall();
            apiCall.type = "recupererNomPrenomPersonnes";
            apiCall.valeur = new HashMap<>();
            apiCall.valeur.put("valeur", 0);
            r = (JSONObject) engine.execute(em, apiCall);
            SuperLogger.println(r.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @org.junit.Test()
    public void testSelectDeuxObjetsAvecJointure() throws Exception {

        SuperLogger.println("Debut test");

        try {
            JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");

            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
            Types types = model.model;
            EntitiesDef entitiesDef = new EntitiesDef(types);
            ClassPool classPool = new ClassPool();

            classPool.appendSystemPath();
            String pckg = "model.jpa.testjointure";
            entitiesDef.generate(pckg, classPool);

            HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();

            EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
            JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
            JpaEntityManagerFactory.auto = "create";
            EntityManager em = factory.getEntityManager();
            //Object o = cls.getConstructors()[0].newInstance();
            List<APIRest> list = model.apis;

            APIDef apiDef = new APIDef(entitiesDef, list, em);
            Engine engine = new Engine(entitiesDef, apiDef, pckg);

            APICall apiCall = new APICall();
            apiCall.type = "voitureEtMaisonPourMemeProprietaire";
            apiCall.valeur = new HashMap<>();
            // apiCall.valeur.put("valeur", 0);
            API api = apiDef.recupererAPI(apiCall.type);
            api.mode = ContextEntityToJson.Mode.Deep;

            Assert.assertEquals(api.getMessageErreur(), null);

            SuperLogger.println(api.getApiDescription().getRetour());
            Assert.assertNotNull(api);
            em.getTransaction().begin();

            JSONObject voitures[] = new JSONObject[]{
                JsonTool.loadJSONObject("/v0.json"),
                JsonTool.loadJSONObject("/v1.json"),
                JsonTool.loadJSONObject("/v2.json")
            };
            JSONObject maisons[] = new JSONObject[]{
                JsonTool.loadJSONObject("/m0.json"),
                JsonTool.loadJSONObject("/m1.json"),
                JsonTool.loadJSONObject("/m2.json")
            };
            JSONObject personnes[] = new JSONObject[]{
                JsonTool.loadJSONObject("/p1.json"),
                JsonTool.loadJSONObject("/p2.json"),
                JsonTool.loadJSONObject("/p4.json")
            };
            List<String> ids = new ArrayList<>();
            //em.persist(o);
            try {
                for (JSONObject p : personnes) {
                    String id = engine.persist(em, p, true).toString();
                    SuperLogger.println(id);
                    ids.add(id);
                }
                for (int idx = 0; idx < maisons.length; idx++) {
                    JSONObject maison = maisons[idx];
                    JSONObject voiture = voitures[idx];
                    JSONObject idPersonne = new JSONObject();
                    idPersonne.put(Constantes.ID, ids.get(idx));
                    maison.getJSONObject(Constantes.VALEUR).put("proprietaire", idPersonne);
                    voiture.getJSONObject(Constantes.VALEUR).put("proprietaire", idPersonne);
                    Object id = engine.persist(em, maison, true);
                    SuperLogger.println(id);
                    id = engine.persist(em, voiture, true).toString();
                    SuperLogger.println(id);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            em.getTransaction().commit();
            JSONObject resultat = (JSONObject) engine.execute(em, apiCall);
            JSONArray r = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertTrue(resultat.getJSONObject(Constantes.REFERENCES).length() > 0);
            SuperLogger.println(resultat.toString());
            JSONArray entities = new JSONArray();
            for (int idxObj = 0; idxObj < r.length(); idxObj++) {
                JSONObject tmp = r.getJSONObject(idxObj).getJSONObject(Constantes.VALEUR);
                for (String key : api.descriptionRetour.from.keySet()) {
                    Assert.assertTrue(tmp.optJSONObject(key) != null);
                }
                JSONObject v = tmp.getJSONObject("v");
                JSONObject m = tmp.getJSONObject("m");
                entities.put(v);
                entities.put(m);
                Assert.assertTrue(v.getJSONObject(Constantes.VALEUR).getJSONObject("proprietaire").getString(Constantes.ID).equals(m.getJSONObject(Constantes.VALEUR).getJSONObject("proprietaire").getString(Constantes.ID)));
            }
            resultat.put(Constantes.VALEUR, entities);
            SuperLogger.println(resultat.toString());
            
            SuperLogger.println(r.toString());

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

}
