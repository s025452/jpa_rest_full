/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import jpa.restfull.Engine;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.API;
import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.exception.ControlException;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.type.EntityFieldTypeWithGeneration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

/**
 *
 * @author DAVID
 */
public class TestCreationAvecIndexUnique {

    @org.junit.Test
    public void test() throws Exception {
          try {
  //      SuperLogger.println("Debut test");
        JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        EntityFieldTypeWithGeneration tmp = (EntityFieldTypeWithGeneration) entitiesDef.mapEntityDef.get("Personne").champs.get("nom");
        Assert.assertTrue(tmp.isNotNull());
      

        APIRest apiRest = new APIRest();
        apiRest.requete = "Personne:recupererPersonne";
        apiRest.useIndex = true;
        apiRest.nom = "testIndex";
        model.apis.add(apiRest);
        String pckg = "model.jpa.test.unique";

   

        //Object o = cls.getConstructors()[0].newInstance();
        Install install = new Install();
        install.model = model;
        install.pckg = pckg;
;

      
        Engine engine = new Engine(install);

        EntityManager em = engine.factory.getEntityManager();
  
        Assert.assertTrue(entitiesDef.indexes.get(apiRest.requete) != null );

        JSONObject personne = JsonTool.loadJSONObject("/p0.json");

        em.getTransaction().begin();
        Class cls = Class.forName(pckg + ".Personne");
        Assert.assertTrue(engine.apiDef.mapAPI.get("testIndex").getApiDescription().estUnique);
        //em.persist(o);
      
            Object id = engine.persist(em, personne, true);
            SuperLogger.println("id =" + id);
            em.getTransaction().commit();
            APICall apiCall = new APICall();
            apiCall.type = "testIndex";
            apiCall.valeur = new HashMap<>();
            apiCall.valeur.put("nom", "Toto");
            apiCall.valeur.put("prenom", "Titi");
            JSONObject r = (JSONObject) engine.execute(em, apiCall);
            Assert.assertTrue(r != null);
            em.getTransaction().begin();
            id = engine.persist(em, personne, true);
            SuperLogger.println("id =" + id);
            em.getTransaction().commit();
            SuperLogger.println(cls + " " + id);
        } catch (Exception e) {
            if (e.getClass() == javax.persistence.RollbackException.class) {
                return;
            }
            e.printStackTrace();
//            SuperLogger.println(e.getClass());
           throw e;
        }

    }

}
