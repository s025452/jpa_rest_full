/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import jpa.restfull.Application;
import jpa.restfull.Constantes;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.APICall;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.json.object.client.ResultatInstallation;
import jpa.restfull.model.type.EntityFieldTypeWithGeneration;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;

/**
 *
 * @author david
 */
public class TestApplication {

    @org.junit.Test
    public void testFichier() throws IOException, Exception {
        String fichier = "C:\\Users\\david\\Documents\\jpa_rest_full\\src\\test\\resources\\test.model.json";
        JSONObject jsonObject = new JSONObject(new String(Files.readAllBytes(Paths.get(fichier))));
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        model.url ="test1";
        try {
            Application.demarer(model, 8995, "test");
           
        } catch (Exception e) {
              e.printStackTrace();
        } finally {
             Application.server.stop(0);
              Application.server = null;
        }
    }

    @org.junit.Test
    public void test() throws IOException, Exception {
        SuperLogger.println("Debut");

        JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        model.url ="test2";
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        EntityFieldTypeWithGeneration tmp = (EntityFieldTypeWithGeneration) entitiesDef.mapEntityDef.get("Personne").champs.get("nom");
        Assert.assertTrue(tmp.isNotNull());

        APIRest apiRest = new APIRest();
        apiRest.requete = "Personne:recupererPersonne";
        apiRest.useIndex = true;
        apiRest.nom = "testIndex";
        model.apis.add(apiRest);

        apiRest = new APIRest();
        apiRest.requete = "Select p from Personne p where p.id = :id ";
        apiRest.useIndex = false;
        apiRest.nom = "recupererParId";
        model.apis.add(apiRest);

        Application.demarer(model, 8900, "test");
        try {

            JSONObject personne = JsonTool.loadJSONObject("/p0.json");
            String r = ApplicationUtils.persist("http://localhost:8900/persist", personne);
            APICall apic = new APICall();
            apic.type = "recupererParId";
            apic.valeur.put("id", r);
            JSONObject apiResult = ApplicationUtils.api("http://localhost:8900/api", apic);
            JSONArray jSONArray = apiResult.getJSONArray(Constantes.VALEUR);
            Assert.assertTrue(jSONArray.length() == 1);
            Assert.assertEquals(jSONArray.getJSONObject(0).getJSONObject(Constantes.VALEUR).getString(Constantes.ID), r);
            apic = new APICall();
            apic.type = "testIndex";
            apic.valeur.put("nom", "Toto");
            apic.valeur.put("prenom", "Titi");
            jsonObject = ApplicationUtils.apiUnique("http://localhost:8900/api", apic);

            Assert.assertEquals(jsonObject.getJSONObject(Constantes.VALEUR).getJSONObject(Constantes.VALEUR).getString(Constantes.ID), r);
            apic = new APICall();
            apic.type = "testIndex";
            apic.valeur.put("nom", "SSS");
            apic.valeur.put("prenom", "Titi");
            jsonObject = ApplicationUtils.apiUnique("http://localhost:8900/api", apic);
            Assert.assertEquals(jsonObject, null);
            SuperLogger.println("Fin");

        } finally {
            Application.server.stop(0);
            Application.server = null;
        }

    }

}
