package jpa.restfulljpa.restfull.test;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import com.sun.net.httpserver.HttpHandler;
import static jpa.restfull.EntityTreeHttpHandler.server;

import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.model.json.object.Types;
import com.sun.net.httpserver.HttpHandler;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import jpa.restfull.Application;
import static jpa.restfull.EntityTreeHttpHandler.server;

import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.ThreadPerTaskExecutor;
import jpa.restfull.model.json.object.Types;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;



public class Test implements HttpHandler {

    static public void main(String args[]) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, MalformedURLException, URISyntaxException, NoSuchFieldException, IOException {
        System.out.println(" nbArgs = " + args.length);
        HttpServer server = HttpServer.create(new InetSocketAddress(6000), 0);

        server.setExecutor(new ThreadPerTaskExecutor()); // creates a default executor
        server.start();
        server.createContext("/a", new Test());
        server.createContext("/b", new Test());
        URL url = new URL("http://localhost:6000/b");
        InputStream is = url.openStream();
        byte bytes[] = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int n;
        while ((n = is.read(bytes)) > 0) {
            baos.write(bytes, 0, n);
        }
        System.out.println(new String(baos.toByteArray()));
        server.stop(0);

    }

    @Override
    public void handle(HttpExchange t) throws IOException {
        String r = "reponse";

        if (t.getRequestURI().toString().equals("/a")) {
            appelA(t);
        }
        if (t.getRequestURI().toString().equals("/b")) {
            appelB(t);
        }
    }

    public void appelA(HttpExchange t) throws IOException {
        String r = "reponse a";

        byte bytes[] = r.getBytes();
        t.sendResponseHeaders(200, bytes.length);
        OutputStream os = t.getResponseBody();
        os.write(bytes);
        os.flush();
        os.close();

    }

    public void appelB(HttpExchange t) throws IOException {
        HttpGet httpGet = new HttpGet("http://localhost:6000/a");
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(httpGet)) {

            String resultat = EntityUtils.toString(response.getEntity());
            byte bytes[] = resultat.getBytes();
            t.sendResponseHeaders(200, bytes.length);
            OutputStream os = t.getResponseBody();
            os.write(bytes);
            os.flush();
            os.close();
        }
    }

}
