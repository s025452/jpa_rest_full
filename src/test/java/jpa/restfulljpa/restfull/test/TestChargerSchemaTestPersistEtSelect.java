package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.Select;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;

public class TestChargerSchemaTestPersistEtSelect {

	
	@Test
	public void test() throws JsonMappingException, JsonProcessingException, IOException, ModelError, CannotCompileException, NotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, URISyntaxException, NoSuchFieldException, NoSuchMethodException, Exception {
		
		JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
		ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
		Model model = (Model)contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
		Types types =model.model;
		EntitiesDef entitiesDef = new EntitiesDef(types);
		Assert.assertTrue(entitiesDef.mapEntityDef.size() == 4);
		EntityDef entityDef = entitiesDef.mapEntityDef.get("Personne");
		Assert.assertTrue( entityDef.champs.size() == 5);
		Assert.assertTrue(entityDef != null );
		Assert.assertTrue(entityDef.entityId != null);
		Assert.assertTrue(entityDef.entityId.isGenerated());
		
		ClassPool classPool =new ClassPool();
		
		String cheminClasses ="c:/tmp/classes/"+this.getClass().getSimpleName();
		
		classPool.appendSystemPath();
//		classPool.
		entitiesDef.generate("model3.jpa", classPool);
	           EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
		JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
		JpaEntityManagerFactory.auto = "create";
		EntityManager em = factory.getEntityManager();
		//Object o = cls.getConstructors()[0].newInstance();
		Engine engine = new Engine(entitiesDef,null,"model3.jpa");
		
		JSONObject personne =	JsonTool.loadJSONObject("/p0.json");
		
		em.getTransaction().begin();
		//em.persist(o);
		Object id = engine.persist(em, personne,true);
		em.getTransaction().commit();
		
		Assert.assertTrue(id != null);
		Select select  = new Select();
		select.query = "Select p from Personne p";
		JSONArray jsonArray = engine.select(em, select,ContextEntityToJson.Mode.Deep);
		Assert.assertTrue( jsonArray.length() == 1);
		System.out.println( id);
		System.out.println( jsonArray);
		select.query = "Select p from Personne p where p.id = :id";
		select.params = new HashMap<>();
		select.params.put("id",id);
		jsonArray = engine.select(em, select,ContextEntityToJson.Mode.Deep);
		Assert.assertTrue( jsonArray.length() == 1);
		
		
	}
	
	
}
