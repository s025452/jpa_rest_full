package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import jpa.restfull.Constantes;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.Select;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;

public class TestChargerSchemaTestListRelation {

	
	@Test
	public void test() throws JsonMappingException, JsonProcessingException, IOException, ModelError, CannotCompileException, NotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, URISyntaxException, NoSuchFieldException, NoSuchMethodException, Exception {
		
		JSONObject jsonObject = JsonTool.loadJSONObject("/test2.model.json");
		ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
		Model model = (Model)contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
		Types types = model.model;
		EntitiesDef entitiesDef = new EntitiesDef(types);
		
		ClassPool classPool =new ClassPool();
		
		String cheminClasses ="c:/tmp/classes/"+this.getClass().getSimpleName()+"/";
		
		classPool.appendSystemPath();
//		classPool.
		entitiesDef.generate("model2.jpa", classPool);
	           EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
		JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
		JpaEntityManagerFactory.auto = "create";
		EntityManager em = factory.getEntityManager();
		//Object o = cls.getConstructors()[0].newInstance();
		Engine engine = new Engine(entitiesDef,null,"model2.jpa");
		em.getTransaction().begin();
		//em.persist(o);
		JSONArray adherents = JsonTool.loadJSONArray("/adherents.json");
		JSONArray clubs = JsonTool.loadJSONArray("/clubs.json");
		List<Object> listIdAdherents = new ArrayList<>();
		for(int i=0;i<adherents.length();i++) {
			listIdAdherents.add(engine.persist(em,adherents.getJSONObject(i),true));
		}
		List<Object> listIdClubs = new ArrayList<>();
		for(int i=0;i<clubs.length();i++) {
			listIdClubs.add(engine.persist(em,clubs.getJSONObject(i),true));
		}
		em.getTransaction().commit();
		em.getTransaction().begin();
		System.out.println( listIdAdherents);
		System.out.println( listIdClubs);
		int n= listIdAdherents.size();
		for(Object id:listIdClubs) {
			for(int i=0;i < n;i++) {
				Object idAdherent = listIdAdherents.get(i);
				JSONObject  relation= new JSONObject();
				relation.put(Constantes.TYPE, "Adhesion");
				JSONObject valeur = new JSONObject();
				relation.put(Constantes.VALEUR,valeur);
				valeur.put("personne",new JSONObject().put(Constantes.ID, idAdherent));
				valeur.put("club",new JSONObject().put(Constantes.ID, id));
				engine.persist(em, relation,true);
				System.out.println(relation);
			}
			n--;
		}
		em.getTransaction().commit();
		
		Select select = new Select();
		select.query = "Select p from Personne p";
		JSONArray r =engine.select(em, select,ContextEntityToJson.Mode.Deep);
		System.out.println(r);
		
		
		
	}
	
	
}
