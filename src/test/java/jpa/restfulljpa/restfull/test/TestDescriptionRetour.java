/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.api.SelectDescriptionRetour;
import jpa.restfull.model.api.StructureRetour;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJavaToJson;
import junit.framework.Assert;
import org.eclipse.persistence.jpa.jpql.parser.Expression;
import org.eclipse.persistence.jpa.jpql.parser.ExpressionVisitor;
import org.eclipse.persistence.jpa.jpql.parser.JPQLExpression;
import org.eclipse.persistence.jpa.jpql.parser.JPQLGrammar2_1;

import static org.junit.Assert.*;

/**
 *
 * @author david
 */
public class TestDescriptionRetour {

    @org.junit.Test
    public void testDescriptionRetour() throws Exception {
        String qs = "Select AVG ( p.k ) , SUM(p.o) , p,e.l ,  p.salaire,p.nom , p.entreprise.nom,COUNT(p.id) from Personne p , joiki e where p.entreprise.id > :entreprise";

        SelectDescriptionRetour descriptionRetour = new SelectDescriptionRetour(qs);
        assertTrue(descriptionRetour.retour.get("AVG").valeurs.get("p").valeurs.get("k").idx == 0);
        assertTrue(descriptionRetour.retour.get("SUM").valeurs.get("p").valeurs.get("o").idx == 1);
        assertTrue(descriptionRetour.retour.get("p").idx == 2);
        assertTrue(descriptionRetour.retour.get("e").valeurs.get("l").idx == 3);
        ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
        System.out.println(contextJavaToJson.create(descriptionRetour));
        ObjectMapper om = new ObjectMapper();
        System.out.println(om.writeValueAsString(descriptionRetour));
    }

    @org.junit.Test
    public void testRequeteSimple() throws Exception {
        String qs = " Select p from Personne p";
        SelectDescriptionRetour descriptionRetour = new SelectDescriptionRetour(qs);
        ObjectMapper om = new ObjectMapper();
        System.out.println(om.writeValueAsString(descriptionRetour));

    }

    public List<Expression> asList(Expression expression) {
        List<Expression> lst = new ArrayList<>();
        for (Expression e : expression.orderedChildren()) {
            lst.add(e);
            //SuperLogger.println(e);

        }
        return lst;

    }
}
