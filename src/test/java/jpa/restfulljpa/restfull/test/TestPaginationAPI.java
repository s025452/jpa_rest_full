/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipOutputStream;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import jpa.restfull.Constantes;
import jpa.restfull.CustomClassLoader;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.*;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

/**
 *
 * @author DAVID
 */
public class TestPaginationAPI {




    @org.junit.Test()
    public void testMethod() throws Exception {
     

        SuperLogger.println("Debut test");

        try {
            JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");

            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
            Types types = model.model;
            EntitiesDef entitiesDef = new EntitiesDef(types);
            ClassPool classPool = new ClassPool();

           

            classPool.appendSystemPath();
            String pckg = "model.jpa.testpage";
            entitiesDef.generate(pckg,  classPool);

            HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();
   
            EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
            JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
            JpaEntityManagerFactory.auto = "create";
            EntityManager em = factory.getEntityManager();
            //Object o = cls.getConstructors()[0].newInstance();
            List<APIRest> list = model.apis;

            APIDef apiDef = new APIDef(entitiesDef, list, em);
            Engine engine = new Engine(entitiesDef, apiDef, pckg);

            JSONObject personne = JsonTool.loadJSONObject("/p0.json");
            APICall apiCall = new APICall();
            apiCall.type = "recupererPersonnes";
            apiCall.valeur = new HashMap<>();
            apiCall.valeur.put("valeur", 0);
            apiCall.sizePage = 1;
            API api = apiDef.recupererAPI(apiCall.type);
            Assert.assertEquals(api.getMessageErreur(), null);

            Assert.assertEquals(api.getApiDescription().getParameters().get("valeur"), "number");

            SuperLogger.println(api.getApiDescription().getRetour());
            Assert.assertNotNull(api);
            em.getTransaction().begin();
            //em.persist(o);
            try {
                SuperLogger.println( engine.persist(em, personne, true));
                SuperLogger.println( engine.persist(em, JsonTool.loadJSONObject("/p1.json"), true));
                SuperLogger.println( engine.persist(em, JsonTool.loadJSONObject("/p2.json"), true));
                SuperLogger.println( engine.persist(em, JsonTool.loadJSONObject("/p4.json"), true));
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            em.getTransaction().commit();
            JSONObject resultat = (JSONObject)engine.execute(em, apiCall);
            JSONArray r = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertEquals(r.length(), 1);
            SuperLogger.println(r);
            apiCall.sizePage =2 ;
            resultat = (JSONObject)engine.execute(em, apiCall);
             r = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertEquals(r.length(), 2);
            
            apiCall.sizePage = 1;
            apiCall.idxPage = 0;
            resultat = (JSONObject)engine.execute(em, apiCall);
             r = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertEquals(r.length(), 1);
            
            String id0 = r.getJSONObject(0).getJSONObject("valeur").getString("id");
            apiCall.idxPage = 1;
      
       resultat = (JSONObject)engine.execute(em, apiCall);
             r = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertEquals(r.length(), 1);
            
            String id1 = r.getJSONObject(0).getJSONObject("valeur").getString("id");
            Assert.assertNotEquals(id0,id1);
            
            
            
            api.setMode(ContextEntityToJson.Mode.NoID);
                apiCall.sizePage =1 ;
            resultat = (JSONObject)engine.execute(em, apiCall);
             r = resultat.getJSONArray(Constantes.VALEUR);
            Assert.assertEquals(r.length(), 1);
            SuperLogger.println(r);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

}
