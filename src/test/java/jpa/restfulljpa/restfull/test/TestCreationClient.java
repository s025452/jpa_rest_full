/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.util.ArrayList;
import jpa.restfull.CustomClassLoader;
import jpa.restfull.Engine;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.client.*;
import jpa.restfull.model.json.object.client.Client;
import org.json.JSONArray;
import org.junit.Assert;
import org.json.JSONObject;

/**
 *
 * @author DAVID
 */
public class TestCreationClient {

    @org.junit.Test
    public void testCreationClient() throws Exception {
        CustomClassLoader customClassLoader = new CustomClassLoader();

        JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);

        Install install = new Install();
        install.model = model;
        install.pckg = "model6.jpa";

        Engine engine = new Engine(install);
        Client client = engine.client();
        int test = 0;
        for (Type t : client.types) {
            if (t.nom.equals("Personne")) {
                Assert.assertEquals(t.champs.size(), 5);
                test++;
            }
            if (t.nom.equals("Entreprise")) {
                Assert.assertEquals(t.champs.size(), 3);
                test++;
            }
            if (t.nom.equals("ReponsevoitureEtMaisonPourMemeProprietaire")) {
                test++;
                
            }
           // Assert.assertTrue(t.champs.size() > 0);
        }
        Assert.assertEquals(test, 3);
        ApiSignature apiSignature = client.signatures.stream().filter((api) -> api.typeQuestion.equals("recupererNomPrenomPersonnes")).findFirst().get();
        Assert.assertTrue(apiSignature.selectDescriptionRetour != null);
        Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p") != null);

        Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("nom") != null);
        Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("prenom") != null);
        Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("nom").idx == 0);
        Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("prenom").idx == 1);

        ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
        try {
            String s = contextJavaToJson.create(client).toString();
            contextJsonToJava = new ContextJsonToJava();
            SuperLogger.println(s);
            client = (Client) contextJsonToJava.create(new JSONArray(s), "jpa.restfull.model.json.object.client");
            apiSignature = client.signatures.stream().filter((api) -> api.typeQuestion.equals("recupererNomPrenomPersonnes")).findFirst().get();
            Assert.assertTrue(apiSignature.selectDescriptionRetour != null);
            Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p") != null);

            Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("nom") != null);
            Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("prenom") != null);
            Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("nom").idx == 0);
            Assert.assertTrue(apiSignature.selectDescriptionRetour.retour.get("p").valeurs.get("prenom").idx == 1);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

}
