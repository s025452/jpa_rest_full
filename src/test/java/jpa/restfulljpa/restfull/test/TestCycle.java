package jpa.restfulljpa.restfull.test;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;

import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfulljpa.restfull.test.model.A;
import jpa.restfulljpa.restfull.test.model.B;
import jpa.restfulljpa.restfull.test.model.C;
import jpa.restfulljpa.restfull.test.model.D;
import jpa.restfulljpa.restfull.test.model.E;
import jpa.restfulljpa.restfull.test.model.F;
import jpa.restfulljpa.restfull.test.model.H;
import jpa.restfulljpa.restfull.test.model.I;

public class TestCycle {
		@Test
		public void test() throws Exception {
			A a= new A();
			a.valeur = false;
			a.unC = new C();
			a.unC.unB = new B();
			a.unC.unB.nom="machain";
			a.unC.unB.valeur = 5;
			a.unC.unB.unA = a;
			a.unG = new F();
			ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
			JSONArray jsonArray = contextJavaToJson.create(a);
			assertTrue(jsonArray.length() == 1);
			ContextJsonToJava contextJsonToJava =new ContextJsonToJava();
			Object r = contextJsonToJava.create(jsonArray, "jpa.restfulljpa.restfull.test.model");
			assertTrue( r != null);
			assertTrue(r instanceof A);
			a = (A) r;
			assertTrue(a.unC.unB.unA == a);
			assertTrue("machain".equals(a.unC.unB.nom) );
			assertTrue(a.unG != null);
			assertTrue(a.unG instanceof F);
			
			D unD = new D();
			unD.unA =a;
			unD.value = 33;
			jsonArray = contextJavaToJson.create(unD);
			assertTrue(jsonArray.length() == 2);
			System.out.println(jsonArray);
			
			 r = contextJsonToJava.create(jsonArray, "jpa.restfulljpa.restfull.test.model");
			assertTrue( r != null);
			assertTrue(r instanceof D);
			unD = (D) r;
			assertTrue(unD.unA.unC.unB.unA == unD.unA);
			E unE = new E();
			unE.listA = new ArrayList<>();
			unE.listA.add(a);
			unE.m = "message";
			jsonArray = contextJavaToJson.create(unE);
			assertTrue(jsonArray.length() == 2);
			r = contextJsonToJava.create(jsonArray, "jpa.restfulljpa.restfull.test.model");
								
			assertTrue( r != null);
			assertTrue(r instanceof E);
			unE = (E) r;
			assertTrue(unE.listA.size() == 1);
			a = unE.listA.get(0);
			assertTrue(a.unC.unB.unA == a);			
			a.unG =a ;
			unD.unA = a;
			jsonArray = contextJavaToJson.create(unD);
			assertTrue(jsonArray.length() == 2);
			r = contextJsonToJava.create(jsonArray, "jpa.restfulljpa.restfull.test.model");
			unD = (D) r;	
			assertTrue(unD.unA.unG != null);
			assertTrue(unD.unA.unG instanceof A);
			
			H unH = new H();
			unH.m = "XX";
			unH.listG = new ArrayList<>();
			unH.listG.add(a);
			F unF = new F();
			unF.nom = "v666";
			unH.listG.add(unF);
			jsonArray = contextJavaToJson.create(unH);
			assertTrue(jsonArray.length() == 2);
			r = contextJsonToJava.create(jsonArray, "jpa.restfulljpa.restfull.test.model");
			unH = (H) r;
			assertTrue(unH.listG.size() ==2);
			assertTrue(unH.listG.get(0) != null);
			assertTrue(unH.listG.get(1) != null);
			assertTrue(unH.listG.get(0) instanceof A);
			assertTrue(unH.listG.get(1) instanceof F);
			unF = (F) unH.listG.get(1);
			assertTrue( "v666".equals(unF.nom));
                        
                        I i = new I();
                        i.titi = new HashMap<>();
                       i.titi.put("nini", unF);
                       jsonArray=contextJavaToJson.create(i);
                       I unI=(I)contextJsonToJava.create(jsonArray, "jpa.restfulljpa.restfull.test.model");
                       	assertTrue( "v666".equals(i.titi.get("nini").nom));
                        
                        
			
			
		}
}
