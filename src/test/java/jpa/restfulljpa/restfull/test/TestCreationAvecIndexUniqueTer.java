/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import jpa.restfull.Engine;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.API;
import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ControlException;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Install;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.type.EntityFieldTypeWithGeneration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

/**
 *
 * @author DAVID
 */
public class TestCreationAvecIndexUniqueTer {

    @org.junit.Test
    public void test() throws Exception {
        //  SuperLogger.println("Debut test");
        try {
            JSONObject jsonObject = JsonTool.loadJSONObject("/data.v2.model.json");
            ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
            Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
            Types types = model.model;
            EntitiesDef entitiesDef = new EntitiesDef(types);

            Install install = new Install();

            install.model = model;
            install.pckg = "model9.jpa";
            HibernatePersistenceUnitInfo.JarFileUrls = new ArrayList<>();

            Engine engine = new Engine(install);
            Assert.assertEquals(engine.erreurs.size(), 0);

        } catch (Throwable e) {
            System.out.println("STACK");
            e.printStackTrace();
            throw e;
        }

    }

}
