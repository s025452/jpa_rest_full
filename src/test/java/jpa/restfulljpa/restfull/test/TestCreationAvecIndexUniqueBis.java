/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.API;
import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ControlException;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.type.EntityFieldTypeWithGeneration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;

/**
 *
 * @author DAVID
 */
public class TestCreationAvecIndexUniqueBis {
    @Ignore()
    @org.junit.Test
    public void test() throws Exception {
      //  SuperLogger.println("Debut test");
     try {
         JSONObject jsonObject = JsonTool.loadJSONObject("/data.v1.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        EntityDef ed = entitiesDef.mapEntityDef.get("Sou");
        Assert.assertTrue(ed != null);
        Assert.assertEquals(ed.champs.size(), 5);
        
        ClassPool classPool = new ClassPool();

        String cheminClasses = "c:/tmp/classes/" + this.getClass().getSimpleName() + "/";

        classPool.appendSystemPath();
        String pckg = "model.jpa.test4bis";
        
        entitiesDef.generate(pckg,  classPool);
         EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
        JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
        JpaEntityManagerFactory.auto = "create";
        EntityManager em = factory.getEntityManager();
        //Object o = cls.getConstructors()[0].newInstance();
        Engine engine = new Engine(entitiesDef,null, pckg);
        } catch( Throwable e) {
            System.out.println( e.toString());
            e.printStackTrace();
            throw e;
        }
        

    }

}
