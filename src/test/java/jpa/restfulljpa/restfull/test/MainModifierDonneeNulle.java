/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import jpa.restfull.Constantes;
import jpa.restfull.model.json.convert.ContextJavaToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Champ;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.TypeDef;
import jpa.restfull.model.json.object.TypeDefRacine;
import jpa.restfull.model.json.object.TypeRef;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author david
 */
public class MainModifierDonneeNulle {

    public static void main(String args[]) throws IOException, Exception {
        String source = new String(Files.readAllBytes(Paths.get("C:\\Users\\david\\Documents\\alimentation-sport-sante-data-model\\data.v1.model.json")));
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(new JSONObject(source), "jpa.restfull.model.json.object", Model.class);
        for(TypeDefRacine t:model.model.types) {
            modifier(t);
        }
        ContextJavaToJson contextJavaToJson = new ContextJavaToJson();
        JSONArray r=contextJavaToJson.create(model);
        JSONObject o= r.getJSONObject(0).getJSONObject(Constantes.VALEUR);
        Files.write(Paths.get("C:\\Users\\david\\Documents\\alimentation-sport-sante-data-model\\data.v2.model.json"), o.toString().getBytes("UTF8"),StandardOpenOption.CREATE);
        System.out.println(r.length());
        
    }

    public static void modifier(TypeDef typeDef) {
        if (typeDef.sousTypes != null) {
            for (TypeDef sousType : typeDef.sousTypes) {
                modifier(sousType);
            }

        }
        for(Champ champ:typeDef.champs) {
            if (champ.type instanceof TypeRef) {
                TypeRef type = (TypeRef) champ.type;
                if (type.noInsert == null) {
                    type.noInsert = true;
                }
                if (type.noUpdate == null) {
                    type.noUpdate = true;
                }
                
            }
        }
    }

}
