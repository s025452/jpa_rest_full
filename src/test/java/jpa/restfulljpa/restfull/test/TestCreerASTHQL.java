package jpa.restfulljpa.restfull.test;

import antlr.BaseAST;
import antlr.collections.AST;
import jpa.restfull.JsonTool;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;

import org.hibernate.engine.query.spi.NamedParameterDescriptor;
import org.hibernate.hql.internal.ast.QueryTranslatorImpl;
import org.hibernate.internal.CoreMessageLogger;
import org.hibernate.internal.HEMLogging;
import org.hibernate.query.internal.QueryImpl;
import org.json.JSONObject;
import org.junit.Assert;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.NotFoundException;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.APIDescription;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.exception.TypeNonValidePourAPI;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import org.eclipse.persistence.jpa.jpql.parser.Expression;
import org.eclipse.persistence.jpa.jpql.parser.JPQLExpression;
import org.eclipse.persistence.jpa.jpql.parser.JPQLGrammar2_1;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.hql.internal.ast.ASTQueryTranslatorFactory;
import org.hibernate.hql.internal.ast.tree.AggregatedSelectExpression;
import org.hibernate.hql.internal.ast.tree.SelectClause;
import org.hibernate.hql.internal.ast.tree.SelectExpression;
import org.hibernate.hql.spi.QueryTranslatorFactory;

public class TestCreerASTHQL {

    @org.junit.Test
    public void test() throws Exception {
        //Enumeration<String> e=java.util.logging.LogManager.getLogManager().getLoggerNames();

        SuperLogger.println("Debut");
        JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        Assert.assertTrue(entitiesDef.mapEntityDef.size() == 4);
        EntityDef entityDef = entitiesDef.mapEntityDef.get("Personne");
        Assert.assertTrue(entityDef.champs.size() == 5);
        Assert.assertTrue(entityDef != null);
        Assert.assertTrue(entityDef.entityId != null);
        Assert.assertTrue(entityDef.entityId.isGenerated());

        ClassPool classPool = new ClassPool();

        String cheminClasses = "c:/tmp/classes/" + this.getClass().getSimpleName() + "/";

        classPool.appendSystemPath();
//		classPool.
        entitiesDef.generate("model.jpa3", classPool);

        EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
        JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
        JpaEntityManagerFactory.auto = "create";

        EntityManager em = factory.getEntityManager();

        try {
            String qs = "Select p,  p.salaire,p.nom , p.entreprise.nom,COUNT(p.id) from Personne p where p.entreprise.id > :entreprise";
         
            JPQLExpression jpql = new JPQLExpression(qs, new JPQLGrammar2_1());
            List<Expression> lst = asList(jpql.getQueryStatement());
            afficher(jpql.getQueryStatement());
            // selectClause.getFirstChild()
            //  SelectExpression selectExpressions[] =   selectClause.collectSelectExpressions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ;
private QueryTranslatorImpl compileQuery(String hql, EntityManager em) {
        QueryTranslatorFactory ast = new ASTQueryTranslatorFactory();
        Session session = em.unwrap(org.hibernate.Session.class);
        SessionFactory factory = session.getSessionFactory();
        QueryTranslatorImpl newQueryTranslator = (QueryTranslatorImpl) ast.createQueryTranslator(hql, hql, Collections.EMPTY_MAP, (SessionFactoryImplementor) factory, null);
        newQueryTranslator.compile(Collections.emptyMap(), false);
        return newQueryTranslator;
    }

    public List<Expression> asList(Expression expression) {
         List<Expression> lst = new ArrayList<>();
            for (Expression e : expression.orderedChildren()) {
                lst.add(e);
                //SuperLogger.println(e);

            }
          return lst;

    }
    
    public void afficher(Expression e) {
        List<Expression> ls = asList(e);
        if (ls.isEmpty()) {
            String s =e.toActualText().trim();
            if (!s.isEmpty()) {
            SuperLogger.println(s); }
        } else {
            for(Expression enfant:ls) {
                afficher(enfant);
            }
        }
    }
}
