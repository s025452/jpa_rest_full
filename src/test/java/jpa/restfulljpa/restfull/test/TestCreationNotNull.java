/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.restfulljpa.restfull.test;

import jpa.restfull.JsonTool;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javassist.ClassPool;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import jpa.restfull.Engine;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.API;
import jpa.restfull.model.APICall;
import jpa.restfull.model.APIDef;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.exception.ControlException;
import jpa.restfull.model.json.convert.ContextEntityToJson;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.APIRest;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import jpa.restfull.model.type.EntityFieldTypeWithGeneration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

/**
 *
 * @author DAVID
 */
public class TestCreationNotNull {

    @org.junit.Test
    public void test() throws Exception {
        SuperLogger.println("Debut test");
        JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        EntityFieldTypeWithGeneration tmp = (EntityFieldTypeWithGeneration) entitiesDef.mapEntityDef.get("Personne").champs.get("nom");
        Assert.assertTrue(tmp.isNotNull());
        ClassPool classPool = new ClassPool();

        String cheminClasses = "c:/tmp/classes/" + this.getClass().getSimpleName() + "/";

        classPool.appendSystemPath();
        String pckg = "model.jpa.test3";
        entitiesDef.generate(pckg, classPool);
        EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
        JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
        JpaEntityManagerFactory.auto = "create";
        EntityManager em = factory.getEntityManager();
        //Object o = cls.getConstructors()[0].newInstance();
        Engine engine = new Engine(entitiesDef,null, pckg);

        JSONObject personne = JsonTool.loadJSONObject("/p3.json");

        em.getTransaction().begin();
        Class cls = Class.forName(pckg + ".Personne");

        //em.persist(o);
        try {
            Object id = engine.persist(em, personne,true);
            em.getTransaction().commit();
           
            SuperLogger.println(cls + " " + id);
        } catch (Exception e) {
            if (e.getClass() == ControlException.class) {
                return;
            }
            throw e;
        }

    }

}
