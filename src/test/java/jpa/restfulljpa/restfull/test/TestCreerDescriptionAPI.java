package jpa.restfulljpa.restfull.test;

import antlr.BaseAST;
import antlr.collections.AST;
import jpa.restfull.JsonTool;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;

import org.hibernate.engine.query.spi.NamedParameterDescriptor;
import org.hibernate.hql.internal.ast.QueryTranslatorImpl;
import org.hibernate.internal.CoreMessageLogger;
import org.hibernate.internal.HEMLogging;
import org.hibernate.query.internal.QueryImpl;
import org.json.JSONObject;
import org.junit.Assert;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.NotFoundException;
import jpa.restfull.EntityTreeHttpHandler;
import jpa.restfull.HibernatePersistenceUnitInfo;
import jpa.restfull.JpaEntityManagerFactory;
import jpa.restfull.SuperLogger;
import jpa.restfull.model.APIDescription;
import jpa.restfull.model.EntitiesDef;
import jpa.restfull.model.EntityDef;
import jpa.restfull.model.exception.ModelError;
import jpa.restfull.model.exception.TypeNonValidePourAPI;
import jpa.restfull.model.json.convert.ContextJsonToJava;
import jpa.restfull.model.json.object.Model;
import jpa.restfull.model.json.object.Types;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.hql.internal.ast.ASTQueryTranslatorFactory;
import org.hibernate.hql.internal.ast.tree.AggregatedSelectExpression;
import org.hibernate.hql.internal.ast.tree.SelectClause;
import org.hibernate.hql.internal.ast.tree.SelectExpression;
import org.hibernate.hql.spi.QueryTranslatorFactory;

public class TestCreerDescriptionAPI {

    @org.junit.Test
    public void test() throws Exception {
        //Enumeration<String> e=java.util.logging.LogManager.getLogManager().getLoggerNames();

        SuperLogger.println("Debut");
        JSONObject jsonObject = JsonTool.loadJSONObject("/test.model.json");
        ContextJsonToJava contextJsonToJava = new ContextJsonToJava();
        Model model = (Model) contextJsonToJava.create(jsonObject, "jpa.restfull.model.json.object", Model.class);
        Types types = model.model;
        EntitiesDef entitiesDef = new EntitiesDef(types);
        Assert.assertTrue(entitiesDef.mapEntityDef.size() == 4);
        EntityDef entityDef = entitiesDef.mapEntityDef.get("Personne");
        Assert.assertTrue(entityDef.champs.size() == 5);
        Assert.assertTrue(entityDef != null);
        Assert.assertTrue(entityDef.entityId != null);
        Assert.assertTrue(entityDef.entityId.isGenerated());

        ClassPool classPool = new ClassPool();

        String cheminClasses = "c:/tmp/classes/" + this.getClass().getSimpleName() + "/";

        classPool.appendSystemPath();
//		classPool.
        entitiesDef.generate("model.jpa2", classPool);

        EntityTreeHttpHandler.demarer(entitiesDef.entityTree);
        JpaEntityManagerFactory factory = new JpaEntityManagerFactory();
        JpaEntityManagerFactory.auto = "create";

        EntityManager em = factory.getEntityManager();

        Query q = em.createQuery("Select p, p.salaire , p.nom from Personne p where p.salaire > :valeur");

        QueryImpl<?> qi = (QueryImpl<?>) q;
        APIDescription apiDescription = new APIDescription((QueryImpl<?>) q);
        Assert.assertEquals(apiDescription.getParameters().get("valeur"), "number");
        Assert.assertEquals(apiDescription.getRetour().size(), 3);
        Assert.assertEquals(apiDescription.getRetour().get(0), "Personne");
        Assert.assertEquals(apiDescription.getRetour().get(1), "number");
        Assert.assertEquals(apiDescription.getRetour().get(2), "string");

        SuperLogger.println(apiDescription.getRetour());
        SuperLogger.println(apiDescription.getParameters());
        q = em.createQuery("Select p, p.salaire from Personne p where p.entreprise = :entreprise");

        apiDescription = new APIDescription((QueryImpl<?>) q);
        Assert.assertEquals(apiDescription.getParameters().get("entreprise"), "Entreprise");
        SuperLogger.println(apiDescription.getRetour());

        SuperLogger.println(apiDescription.getParameters());
        try {
             //  SelectExpression selectExpressions[] =   selectClause.collectSelectExpressions();
       
            q = em.createQuery("Select p, p.salaire from Personne p where p.entreprise.id > :entreprise");
            //  q.g
            apiDescription = new APIDescription((QueryImpl<?>) q);
            Assert.assertEquals(apiDescription.getParameters().get("entreprise"), "uuid");
            SuperLogger.println(apiDescription.getRetour());
            SuperLogger.println(apiDescription.getParameters());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ;
private QueryTranslatorImpl compileQuery(String hql, EntityManager em) {
        QueryTranslatorFactory ast = new ASTQueryTranslatorFactory();
        Session session = em.unwrap(org.hibernate.Session.class);
        SessionFactory factory = session.getSessionFactory();
        QueryTranslatorImpl newQueryTranslator = (QueryTranslatorImpl) ast.createQueryTranslator(hql, hql, Collections.EMPTY_MAP, (SessionFactoryImplementor) factory, null);
        newQueryTranslator.compile(Collections.emptyMap(), false);
        return newQueryTranslator;
    }

    public void afficher(AST ast) {
        if (ast == null) {
            return;
        }
        if (ast.getFirstChild() == null) {
            SuperLogger.println(ast.getText());
        } 
        afficher(ast.getNextSibling());
        afficher(ast.getFirstChild());
        
    }
}
