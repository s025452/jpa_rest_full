export const META = [
{ nom: 'Saisie',
abstrait: false,
 champs: [
  { nom: 'personnes', type: '*Personne' } ,
]
},
{ nom: 'Travailleur',
abstrait: false,
super: 'Personne',
 champs: [
  { nom: 'salaire', type: 'int' } ,
  { nom: 'patron', type: 'Patron' } ,
]
},
{ nom: 'Patron',
abstrait: false,
super: 'Personne',
 champs: [
  { nom: 'salaries', type: '*Travailleur' } ,
  { nom: 'entreprises', type: '*Entreprise' } ,
]
},
{ nom: 'Personne',
abstrait: false,
 champs: [
  { nom: 'gid', type: 'string' } ,
  { nom: 'nom', type: 'string' } ,
  { nom: 'prenom', type: 'string' } ,
  { nom: 'age', type: 'int' } ,
  { nom: 'profil', type: 'Profil' } ,
]
},
{ nom: 'Entreprise',
abstrait: false,
 champs: [
  { nom: 'patron', type: 'Patron' } ,
  { nom: 'chiffreAffaire', type: 'number' } ,
  { nom: 'production', type: 'string' } ,
]
},
{ nom: 'Profil',
abstrait: false,
 champs: [
  { nom: 'gid', type: 'string' } ,
  { nom: 'religion', type: 'string' } ,
  { nom: 'couleur', type: 'string' } ,
]
}, ];
export class Saisie {
 public personnes: Personne[] ;
 typeName(): string { return 'Saisie' ; }
 init( noeuds: any , references: any []): void {
   this.personnes = creer_Personne_1( noeuds.personnes, references);
}}
export class Personne {
 public gid: string ;
 public nom: string ;
 public prenom: string ;
 public age: number ;
 public profil: Profil ;
 typeName(): string { return 'Personne' ; }
 init( noeuds: any , references: any []): void {
   this.gid = creer_string( noeuds.gid, references);
   this.nom = creer_string( noeuds.nom, references);
   this.prenom = creer_string( noeuds.prenom, references);
   this.age = creer_int( noeuds.age, references);
   this.profil = creer_Profil( noeuds.profil, references);
}}
export class Travailleur extends Personne {
 public salaire: number ;
 public patron: Patron ;
 typeName(): string { return 'Travailleur' ; }
 init( noeuds: any , references: any []): void {
   this.gid = creer_string( noeuds.gid, references);
   this.nom = creer_string( noeuds.nom, references);
   this.prenom = creer_string( noeuds.prenom, references);
   this.age = creer_int( noeuds.age, references);
   this.profil = creer_Profil( noeuds.profil, references);
   this.salaire = creer_int( noeuds.salaire, references);
   this.patron = creer_Patron( noeuds.patron, references);
}}
export class Patron extends Personne {
 public salaries: Travailleur[] ;
 public entreprises: Entreprise[] ;
 typeName(): string { return 'Patron' ; }
 init( noeuds: any , references: any []): void {
   this.gid = creer_string( noeuds.gid, references);
   this.nom = creer_string( noeuds.nom, references);
   this.prenom = creer_string( noeuds.prenom, references);
   this.age = creer_int( noeuds.age, references);
   this.profil = creer_Profil( noeuds.profil, references);
   this.salaries = creer_Travailleur_1( noeuds.salaries, references);
   this.entreprises = creer_Entreprise_1( noeuds.entreprises, references);
}}
export class Entreprise {
 public patron: Patron ;
 public chiffreAffaire: number ;
 public production: string ;
 typeName(): string { return 'Entreprise' ; }
 init( noeuds: any , references: any []): void {
   this.patron = creer_Patron( noeuds.patron, references);
   this.chiffreAffaire = creer_number( noeuds.chiffreAffaire, references);
   this.production = creer_string( noeuds.production, references);
}}
export class Profil {
 public gid: string ;
 public religion: string ;
 public couleur: string ;
 typeName(): string { return 'Profil' ; }
 init( noeuds: any , references: any []): void {
   this.gid = creer_string( noeuds.gid, references);
   this.religion = creer_string( noeuds.religion, references);
   this.couleur = creer_string( noeuds.couleur, references);
}}
export function creer_Personne_1( noeud: any , references: any [] ): Personne[] {
 if (noeud === null || noeud === undefined ) { return null;
}
 const result: Personne[] = [];
 noeud.forEach( (enfant: any) => {
 result.push(creer_Personne(enfant, references)); });
 return result;
}

export function creer_Personne( noeud: any , references: any [] ): Personne {
 if (noeud === null || noeud === undefined ) { return null;
}
 if ( typeof noeud === 'number' ) {
  const obj = references[noeud];
  if ( obj instanceof Personne ) {
    return obj;
   }
  const result1 = new Personne();
  references[noeud] = result1;
  result1.init(obj, references);
  return result1;
 }
 if (noeud.type === 'Personne') {
   const result = new Personne();
   result.init(noeud.valeur, references);
   return result; }
 if (noeud.type === 'Travailleur') {
   const result = new Travailleur();
   result.init(noeud.valeur, references);
   return result; }
 if (noeud.type === 'Patron') {
   const result = new Patron();
   result.init(noeud.valeur, references);
   return result; }
 return undefined;
}

export function creer_int( noeud: any , references: any [] ): number {
 if (noeud === null || noeud === undefined ) { return null;
}
 return noeud;
}export function creer_Patron( noeud: any , references: any [] ): Patron {
 if (noeud === null || noeud === undefined ) { return null;
}
 if ( typeof noeud === 'number' ) {
  const obj = references[noeud];
  if ( obj instanceof Patron ) {
    return obj;
   }
  const result1 = new Patron();
  references[noeud] = result1;
  result1.init(obj, references);
  return result1;
 }
 const result = new Patron();
 result.init(noeud, references);
 return result; }

export function creer_Travailleur_1( noeud: any , references: any [] ): Travailleur[] {
 if (noeud === null || noeud === undefined ) { return null;
}
 const result: Travailleur[] = [];
 noeud.forEach( (enfant: any) => {
 result.push(creer_Travailleur(enfant, references)); });
 return result;
}

export function creer_Travailleur( noeud: any , references: any [] ): Travailleur {
 if (noeud === null || noeud === undefined ) { return null;
}
 if ( typeof noeud === 'number' ) {
  const obj = references[noeud];
  if ( obj instanceof Travailleur ) {
    return obj;
   }
  const result1 = new Travailleur();
  references[noeud] = result1;
  result1.init(obj, references);
  return result1;
 }
 const result = new Travailleur();
 result.init(noeud, references);
 return result; }

export function creer_Entreprise_1( noeud: any , references: any [] ): Entreprise[] {
 if (noeud === null || noeud === undefined ) { return null;
}
 const result: Entreprise[] = [];
 noeud.forEach( (enfant: any) => {
 result.push(creer_Entreprise(enfant, references)); });
 return result;
}

export function creer_Entreprise( noeud: any , references: any [] ): Entreprise {
 if (noeud === null || noeud === undefined ) { return null;
}
 if ( typeof noeud === 'number' ) {
  const obj = references[noeud];
  if ( obj instanceof Entreprise ) {
    return obj;
   }
  const result1 = new Entreprise();
  references[noeud] = result1;
  result1.init(obj, references);
  return result1;
 }
 const result = new Entreprise();
 result.init(noeud, references);
 return result; }

export function creer_string( noeud: any , references: any [] ): string {
 if (noeud === null || noeud === undefined ) { return null;
}
 return noeud;
}export function creer_Profil( noeud: any , references: any [] ): Profil {
 if (noeud === null || noeud === undefined ) { return null;
}
 if ( typeof noeud === 'number' ) {
  const obj = references[noeud];
  if ( obj instanceof Profil ) {
    return obj;
   }
  const result1 = new Profil();
  references[noeud] = result1;
  result1.init(obj, references);
  return result1;
 }
 const result = new Profil();
 result.init(noeud, references);
 return result; }

export function creer_number( noeud: any , references: any [] ): number {
 if (noeud === null || noeud === undefined ) { return null;
}
 return noeud;
}