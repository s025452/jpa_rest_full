import * as m from './test.type';
export function visiterSaisie( obj: m.Saisie ,  v: Visiteur): void {
  if (!obj) { return; }
  v.visiterSaisie(obj);
}
export function visiterTravailleur( obj: m.Travailleur ,  v: Visiteur): void {
  if (!obj) { return; }
  v.visiterTravailleur(obj);
}
export function visiterPatron( obj: m.Patron ,  v: Visiteur): void {
  if (!obj) { return; }
  v.visiterPatron(obj);
}
export function visiterPersonne( obj: m.Personne ,  v: Visiteur): void {
  if (!obj) { return; }
  if (obj.typeName() === 'Travailleur') { v.visiterTravailleur(obj as m.Travailleur ); }
  if (obj.typeName() === 'Patron') { v.visiterPatron(obj as m.Patron ); }
  if (obj.typeName() === 'Personne') { v.visiterPersonne(obj as m.Personne ); }
}
export function visiterEntreprise( obj: m.Entreprise ,  v: Visiteur): void {
  if (!obj) { return; }
  v.visiterEntreprise(obj);
}
export function visiterProfil( obj: m.Profil ,  v: Visiteur): void {
  if (!obj) { return; }
  v.visiterProfil(obj);
}
export class Visiteur {
 visiterSaisie( obj: m.Saisie ) {  }
 visiterTravailleur( obj: m.Travailleur ) {  }
 visiterPatron( obj: m.Patron ) {  }
 visiterPersonne( obj: m.Personne ) {  }
 visiterEntreprise( obj: m.Entreprise ) {  }
 visiterProfil( obj: m.Profil ) {  }
}
